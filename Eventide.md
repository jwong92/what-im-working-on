# Factset Eventide Components
Currently, we use Factset (aka ds1) to import the majority of our data. We do this through components, that represent a specifc business concept in Irwin. For example, People, Organization, or Holdings. Each component does a specific query in the factset db, and each row is processed through the component loader. This loader then decides which events to write to the `message_store`, which is then picked up by a handler, to then write view data commands, which is then further picked up by a view data consumer to write the actual SQL commands to the irwin database.

This process affords us a number of benefits:
1. The process is incredibly streamlined, and when written properly, does not require very much maintenance, or changes in the future
2. The process is effective against network failures. If for whatever reason, the view data consumer is down, view data messages will continue to be written in the message_store, and stay there until the consumer is back up and running. The consumer periodically marks the last position at which they left off, and can pick up from there and continue on with the messages that have piled up.

## Messages
Messages are the overarching name for events and commands. Messages are just data objects. They have attributes and thats it. No capabilities whatsoever. Its sole purpose is to store the information is a JSON object in the message store.

In our ds1 components, we typically have `initiated`, `identified`, `changed`, and sometimes `deleted` and `re_initiated` events. By nature, `initiated` and `identified` events are only ever written *once*, so they are by nature idempotent. `changed`, `deleted` and `re_initiated` events (if available) however can be written a number of times (ie: attributes can consistently change when a data row is being imported). If we decide to use the same component in another service (ie: re-write these messages), we would have no way of telling the difference between the messages. As such, there needs to be a unique identifier between them to be able to distinguish between the two.

### Messaging::Message
This module affords the receiver with:
- The `attribute` macro that helps with defining the event/message attributes
- The `build` constructor that optionally recieves a hash or attribute data and hash of metadata attribute data
- The `follow` constructor that takes a message and constructs another message based on the former messages' data *and* metadata

## Consumers
Consumers read messages from a single *category* and dispatches the message to the handler(s) that have been added to the consumer. A consumer keeps track of its' own progress through the category stream that it is reading from, so it can always pick up where it left off by storing the message position of messages that have already been read.

- A class becomes a consumer by including the `Consumer::Postgres` module
- Reads from a **single** category stream
- Has 1 or more handlers
- Periodically records the readers position ot the mssage store

### `Consumer::Postgres` Module
This module affords the receiver with:
- The `start` method for starting a consumer, and supplying it with arguments that control its' behaviour
- The `handler` class macro for adding handlers to a consumer
- The `identifer` class macro used to declare an arbitrary string suffix used to compose a distinct position stream name used by the position store to store consumer position records

#### Starting a Consumer
Invoke a consumer class's `start` method and pass it the category name the consumer will read messages from

```
SomeConsumer.start('someCategory')

self.start(category, poll_interval_milliseconds: 100, batch_size: 1000, position_update_interval: 100, identifier: nil, correlation: nil, group_member: nil, group_size: nil, condition: nil, settings: nil)
```
## Message DB
### Server Functions
The Message store provides Postgres server functions to be used with any language through the `psql` command line tool. The list of functions include:

- write_message
- get_stream_messages
- get_category_messages
- get_last_stream_messages
- stream_version
- id
- cardinal_id
- category
- is_category
- acquire_lock
- hash_64
- message_store_version

Below is an example of how to use the functions. Reminder that the consumer automatically creates a position message in the message_store that periodically keeps track of where it should be consuming in the stream.

```
-- Remove all stakes import from the message store
DELETE FROM message_store.messages
WHERE message_store.category(stream_name) = 'ds1HoldingStake';

-- Delete view data messages
DELETE FROM message_store.messages
WHERE message_store.category(stream_name) = 'irwinViewDataHoldingStake:command';

-- Reset view data positions
DELETE FROM message_store
WHERE stream_name = 'irwinViewDataHoldingStake:command+position';

DELETE FROM message_store.messages
WHERE stream_name = 'ds1HoldingStake:position-viewDataCommands';
```
