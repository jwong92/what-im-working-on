# Inheritance

Inheritance is when a class inherits behaviours and methods from another class. The class that is inheriting behaviour is the subclass, while the one being inherited from is the superclass.

Inheritance is commonly used to extract common behaviours into their own class so we can keep logic (that might be useful in a number of other subclasses) in one place.

```
class Song
  def genre
    'r&b'
  end
end

class Album < Song
end

class Discography < Album
end

album = Album.new
album.genre
# => 'r&b'

discography = Discography.new
discography.genre
# => 'r&b'
```

Inheritance is denoted using the `<` symbol. In this case, the Album class is inheriting from the Song class, and thus is also inheriting the `genre` method. This would mean that any class that inherits from the Album class, would also inherit from the Song class as well, as depicted by the Discography method.

What if we wanted to ovveride the method supplied to us from an inherited class?

```
class Song
  def genre
    'r&b'
  end
end

class Album < Song
  def genre
    'kpop'
  end
end

song = Song.new
song.genre
# => 'r&b'

album = Album.new
album.genre
# => 'kpop'
```

Notice that while the `Song` class `genre` method continues to return r&b, bu the `Album` class `genre` method returns kpop instead, because it overrides the inherited `genre` method.

This is of course a very oversimplified example, however the core concept is evident. Separating duplicated code into super classes and inheriting from them allows us to DRY our code. That being said, it can become incredibly convoluted and hard to track down when we begin to inherit from super classes that might be 10 ancestors above the current one. Inheritance can certainly be useful for drying up code, while also isolating methods into a single class, but they can also become a place where we try to make methods fit for a variety of other classes that might want to inherit this method, but are just missing a little something.

In ruby, a class can only inherit from a single other class at a time. (ie: a class can inherit from another class, which inherits from another class)

## Super

The `super` keyword is used when a subclass has a method overriden from the inherited superclass. When calling super from within a method, it searches the ancestor chain for a method with the same name. Here's an example:

```
class Parent
  def say(message)
    puts message
  end
end

class Child < Parent
  def say(message)
    super
  end
end

Child.new.say('Hi there')
# => Hi there
```

The `Child` class inerits from the `Parent` class, and includes the `say` method that overrides that of the `Parent` class. As a result, initializing the `Child` class's `say` method will invoke the `Parent`'s `say` method, and print the message.

The super method can also be invoked with arguments, which will also be sent to the parent in the ancestor chain. This way of invoking super with arguments is commonly seen in the initialize method.

```
class Parent
  def initialize(name)
    @name = name
  end
end

class Child < Parent
  def initialize(name, message)
    super(name)

    @message = message
  end

  def first_words
    puts "#{@name} #{@message}"
  end
end

Child.new('mom', 'i love you' ).first_words
```

## Super()

By default, calling super in the child method implicitly passes the arguments to the parent

Super with parenthesis calls the method in the superclass without accepting any arguments. If a superclass has a method that doesn't take arguments, this would be the safest way to initialize it.

```
class Parent
  def initialize
  end
end

class Child < Parent
  def initialize(message)
    super()

    @message = message
  end
end

child = Child.new('hello')
```
