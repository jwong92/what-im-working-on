# Rails Form Validations

These are some of the nicest things that I have found from form validations:

1. Easier to test - Instead of testing the whole controller, we can now isolate the form validation class and test the parameters there instead.
2. Easier to read - We know that all the validation is in one spot. We could also create multiple form validations for each controller
3. Keeps the controller thin - The controller only has to focus on returning the collection

Almost a year ago, we introduced this concept of `Form Validations` into our controllers for rails. Upon first glance, it seemed incredibly daunting to go through. Not only did it make the controller seem more bloated than before, but I also did not understand the concepts behind these validations. Almost a year later, although I have not yet completely understood all the best practices that are associated with these validation classes, I have began to understand some basics, and their value added to the overall security, organization and simplicity of the code.

Similar to how we do validations in the rails `Model` framework, we can include the `ActiveModel::Model` interface to these Form validation classes, and make use of the included methods that would normally only be accessible in the `Model` of the MVC framework.

Including the `ActiveModel::Model` interface allows you to:

- initialize an object with a hash of attributes
- validate attributes using various validators, while also allowing you to define your own custom validators
- attribute accessors
- etc...

A basic form validator that exists in the controller could look like this:

```
class BaseForm
  include ActiveModel::Model

  attr_accessor :name, :age

  validates :age, numericality: true
  validates :name, presence: true, allow_blank: false
end

form = BaseForm.new()
form.name
# => nil
form.valid?
# => false

form = BaseForm.new(name: 'Jessica', age: 25)
form.name
# => 'Jessica'
# => true
```

Notice here we use the `validates` shorthand, which is a shortcut to all default validators that exist in rails (ex: `validates_presence_of`, `validates_numericality_of`, etc...). the `validate` method also includes a number of other options that can be passed in such as `allow_blank`, `allow_nil`, `if`, `unless`, `on` and `strict`.

You can instantiate a new instance of this class with a hash of attributes, and check for the validity by calling `valid?` on the form. This will go through all the validations that you have set up for these attributes and determine if the attributes that have been passed in for this particular instance is valid or not.

If the default validators that rails have aren't enough for your specific use case, you can always use the `validate` method, which accepts a block, and within it can be used to further customize any validations that you might need. See below for an example

```
class BaseForm
  include ActiveModel::Model
  PAYMENT_METHODS = ['visa', 'mastercard', 'american_express']

  attr_accessor :payments

  validate :payment_types

  def payment_types
    return if payments.blank?

    payments.map do |payment|
      errors.add(:payment, 'type is not accepted') unless PAYMENT_METHODS.include?(payment)
    end
  end
end

form = BaseForm.new(payments: ['cash'])
form.valid?
# => false
form.errors
# => @base=#<BaseForm:0x000000010712ecd0 @errors=#<ActiveModel::Errors:0x00000001247d0be8 ...>, @payments=["cash"], @validation_context=nil>,
# => @details={:payment=>[{:error=>"type is not accepted"}]},
# => @messages={:payment=>["type is not accepted"]}>

form = BaseForm.new(payments: ['visa', 'american_express'])
form.valid?
# => true
```

References:

- [ActiveModel::Model](https://api.rubyonrails.org/classes/ActiveModel/Model.html)
- [validates](https://api.rubyonrails.org/classes/ActiveModel/Validations/ClassMethods.html#method-i-validates)
- [validate](https://api.rubyonrails.org/classes/ActiveModel/Validations/ClassMethods.html#method-i-validate)
- [errors](https://api.rubyonrails.org/classes/ActiveModel/Validations.html#method-i-errors)
