# Proc

A `Proc` encapsulates a block of code that can be stored in a local variable, passed to a method, or another Proc to be called.
