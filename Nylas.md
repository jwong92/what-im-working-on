# Nylas

## Upcoming Work

- We should change the relationship between a user (email) and account. It shouldn't just be 1 to 1, but 1 account should have many users
- Attachments - we need a way to be able to save them?

## Architechture Overview

- REST API interface to query the end users email provider inbox
- Nylas gives us an abstraction so we don't have to care abt the end user's email service provider
- We can query info abt the the email and nylas gives us the email details
- When we authenticate a user, we only ask for email permissions, but we can use the calendar or contact integrations as well
- When a user receives/sends an email outside irwin, access to webhooks received from Nylas. We subscribe to events and receive notifications
- One demand request api, and constant pinging fom the webhook

## Irwin Integration

- **Status** - In the settings, where a user authenticates - gives info on the authentication and provider, state of the account (**important**). If a user asks about why the email has stopped working, first take a look at the status of the settings page - if `running` everything is working fine, otherwise if `stopped` or `invalid` then can dig into why
- **External Email Tracking** - Allow users to do external email tracking - keeps track of all email activities outside of irwin. Only logs emails that belongs to an irwin contact
- **Selective Email Tracking** - sending emails outside of irwin and bcc'ing we track those into activities
- Currently no way to un-authenticate the user through settings, so if a customer decides to leave and even if they aren't authenticated or they are no longer paying for irwin mail, if nylas still has them in their system, we need to remove them completely, or else we will keep paying for their user account (aka we pay Nylas per user)

## Nylas Dashboard

- There is irwin (production) and sandbox (staging)
- If a user can't authenticate, look at the `Auth Logs` in the Nylas dashboard.
- Want to clean up any users that are stopped on the dashboard cause we still pay for those
- Every api call we make is logged, and you can see it on the dashboard
- The settings will tell you a lot about why a user can't authenticate as well - we give them basic SMTP connection, but if `exchange_server_host` is seen, the user might have to go back to their IT and get custom configurations
- Can look at a specific account, and there are details on all of the activity for that account. Can look at the webhook logs timestamps that will show when the last time an accounts was synced (ie: maybe the last webhook received was 3 hours ago etc...)
- If a customer has 2FA in outlook, there can be a problem with authentication (but CSM has notes on zendesk about how to do it)

## Externl Email Tracking

- Tracking emails that belong to irwin contacts
- When a user sends an email outside of irwin, we get a webhook - if we have a person that exists, we log it

## Tracking

- Person replies to an email in a thread, and then included bcc in the recipient, but that email has already been tracked, so adding bcc won't add a new activity.
- If you reply to a message in a thread that is old (ie: 3 months ago) and you've had tons of replies already (ie: your thread is like 3 pages long), your sent message will log in the order of date, so it won't show up as the **newest** at the top
- sending emails to bcc, but weren't being logged in irwin because Nylas was dropping all the information (ie headers of bcc/cc) - so we'd never know if bcc was a recipient so we'll never log them.
  - Separate rake task (ev. 15 mins) that comb through the bcc@irwin inbox that then corresponds to the sender and logs the message
  - job is `email-bcc-email-logger` which is an upsert command - this is a backup in case Nylas drops the information or doesn't give it back to us
  - `email-client-data-sync-runner` backup that gets messages since the last time we synced in case the webhook ever goes down - reliable because more relies on polling
    - If there is a failure on our end, we have a cursor that checks the last time we synced messages on our end, so we can always determine from where we need to re-sync from

### Email Activity Tracking

- Every separate email has a `thread_id` that is unique.

### Selective Email Tracking

- Email that doesn't belong to irwin but can bcc us to have that tracked
- When a user sends an email outside of irwin, we get a webhook - if we have a person contains bcc@irwin, we log it as well

## Group Emails

- Can always schedule an email
- There are webservices that take the campaign into the next stage (runner checks every 5 minutes for group emails that are ready to send out `email-start-group-email-deliveries`)
- If you click send now, the max wait time is **5 minutes** because of that job that runs every 5 minutes.
- We create a new record for every single recipient we send the group email out to.
- Can't send out a mass email from your email at once. Can only send out a certain amount of automated messages in a specific amount of time. We have to batch and queue the messages that get sent every 30s - the frequency used to not be blocked by the email provider or spam trackers

## Personalized vs. Bulk

- Personlized is still sending an email to multiple people, but the way we do it in irwin is by bcc-ing the list of people, which makes it seem like you're sending a 1v1 email instead of a mass/bulk one. The issue with this, is we are unable to detect any bounces or opens, because there's no way of knowing who the email was from.

## Authentication

1. Hosted

- Using Nylas's authentication UI to login - redirecting our login to Nylas, and we get an access token in exchange for the hosted authentication.
- It also identified the provider for us as well

2. Native

- Instead of Nylas handling the token exchange and identifying the provider, we do it ourselves, which gives the user a better user experience and workflow, but it would take more work.

## Account Synchronization

1. Cron Job that runs every day to synchronize accounts. It will mark an account invalid if it can't access the account
2. Webhooks catches changes to the account when

- Account is Running
- Account is Invalid
- Account is Stopped

On the Nylas webhook account you can see the triggers

## Disconnecting Accounts

- Now a user can disconnect their email from irwin themselves
- There are some consultants that purchase Irwin, and work for company A, but then stop for working for company B, but still want to use their own email without having to subscribe to the same emails from company A. Allows them to connect another user to another email address.
- This means 1 user = 1 email, but 1 account can have many users and thus many different email accounts linked

## Email Account Models

- `Email::AccountInvalidation` - when an account becomes inactive and if/when the user gets notified
- `Email::AccountSync` - records the latest cursor (last time the account was synced) for each account
- `Email::Account`
