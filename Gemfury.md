# Gemfury

Here are some helpful docs:
- Uploading Packages to Gemfury with Git | [docs](https://gemfury.com/help/git-push-package/)

## Initial Setup
This command only needs to be executed once per repo
`git remote add fury https://git.fury.io/username/package-name.git`
- **username** here can be yours or any of the account usernames in which you are a collborator
- **package-name** should be the name of the package

Once the remote repo is added, you can push updates to the package by running
`git push fury master`
