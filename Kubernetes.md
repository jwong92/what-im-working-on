# Kubernetes

## Creating a New Pod (Object?) in Namespace for Deployment

Here are some resources to documentation that is helpful for this process

- Namespaces Walthrough | [docs](https://kubernetes.io/docs/tasks/administer-cluster/namespaces-walkthrough/)
- Managing Kubernetes Objects | [docs](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/)
- Workload Deployments | [docs](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

1. Check the current context using `kubectl config view` or `kubectl config current-context` to make sure you're in the right namespace

```
apiVersion: v1
clusters:
- cluster:
  certificate-authority-data: REDACTED
  server: https://130.211.122.180
name: lithe-cocoa-92103_kubernetes
contexts:
- context:
  cluster: lithe-cocoa-92103_kubernetes
  user: lithe-cocoa-92103_kubernetes.
name: lithe-cocoa-92103_kubernetes
current-context: lithe-cocoa-92103_kubernetes
kind: Config
preferences: {}
users:
- name: lithe-cocoa-92103_kubernetes
user:
  client-certificate-data: REDACTED
  client-key-data: REDACTED
  token: 65rZW78y8HbwXXtSXuUw9DbP4FLjHi4b
- name: lithe-cocoa-92103_kubernetes-basic-auth
user:
  password: h5M0FtUUIflBSdI7
  username: admin
```

- See `current-context` in the config file. That is the namespace of your current context
- You can add new namespaces found in the **Namespaces Walkthrough** link above
- You can use `kubectl create -f <filename|url>` to create an object from a configuration file

**Examples**

| Imperative Commands                       | Imperative Object Config                                                                                                               | Declarative Object Config                               |
| ----------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------- |
| `kubectl run = kubectl create deployment` | `kubectl create -f your-object-config.yaml`, `kubectl delete -f your-object-config.yaml`, `kubectl replace -f your-object-config.yaml` | `kubectl diff -f configs/`, `kubectl apply -f configs/` |

```
View Objects: kubectl get -f <filename|url> -o yaml

# Note the -o yaml flag specifies the full object configuration in printed. Params are optional
```

If you need to support multiple writers to the same object, you can use `kubectl apply` to manage the object.

**What is the difference between create and apply**

- `kubectl create` is [Imperative Management](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/imperative-config/). You tell the Kubernetes API what you want to create, replace or delete, not how you want your K8s cluster world to look like. This will throw an error if the resource already exists.
- `kubectl apply` is a form of [Declarative Management](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/). Changes that you may have applied to a live object (i.e. through scale) are **maintained** even if you apply other changes to the object. This will do whatever is necessary (create, update, etc) and will not raise an error. It can make incremental changes to an existing object while also creating the object if it does not exist.

|                  | Advantages                                                                                                                                                                                                                                     | Disadvantages                                                                                                                                              |
| ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `kubectl create` | Simpler and easier to understand.                                                                                                                                                                                                              | Works best on files, not directories & Updates to live objects must be reflected in configuration files, or they will be lost during the next replacement. |
| `kubectl apply`  | Changes made directly to live objects are retained, even if they are not merged back into the configuration files. Better support for operating on directories and automatically detecting operation types (create, patch, delete) per-object. | Harder to debug and understand results when they are unexpected. Partial updates using diffs create complex merge and patch operations.                    |

2. Apply manifest to create a deployment `kubectl apply -f <PATH_TO_FILE>.yaml`
3. `kubectl get deployment` - to get information on the deployment. Headers are as follows

| NAME                                                 | READY                                                                                                                                                    | UP-TO-DATE                                                                           | AVAILABLE                                                                  | AGE                                                                |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ | -------------------------------------------------------------------------- | ------------------------------------------------------------------ |
| lists the names of the Deployments in the namespace. | displays how many replicas of the application are available to your users. It follows the pattern ready/desired in your yaml file under `spec: replicas` | displays the number of replicas that have been updated to achieve the desired state. | displays how many replicas of the application are available to your users. | displays the amount of time that the application has been running. |

4. `kubectl rollout status deployment.v1.apps/<NAME_OF_DEPLOYMENT>` gives the status how many pod replicas are being rolled out with the same pod template.

## Creating a New Cron Job

Here are some resources to documentation that is helpful for this process

- Running Automated Tasks With Cron Job | [docs](https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/)

1. Run the cron job using command: `kubectl create -f <PATH_TO_FILE>.yaml`
2. Get cron job status using `kubectl get cronjob <NAME_OF_JOB_OUTPUT_FROM_ABOVE>`
3. Wait and watch for the job to be created in ~1 min `kubectl get jobs --watch`

Can edit the schedule to do a once off run, and then revert the schedule back so that it doesn't run again.

## Configure Access to Multiple Clusters

- Suppose you have 2 clusters, one for staging and one for production. In the staging environment, the namespace being worked on is literally called staging, and production. A configuration file describes clusters, users and contexts.
- You can switch between a staging and production context if you define it in your .kube/config file. This can also be found already written up in rancher that you just need to copy paste.
- `kubectl config current-context` - to determine which context you're in (staging/production)
- `kubectl config use-context <MY-CLUSTER-NAME>` - to switch contexts
- `kubectl get po -n <NAMESPACE>` - will get the pods in that namespace
- `kubectl exec -it <POD> -n <NAMESPACE> -- /bin/bash` - will open up a running pod that you can use to go through
- From here, you can psql into any database

## Notes

Getting Started with `kubectl`

- Create a new file in `~/.kube/config` and make sure the configuration is up to date (things can change from time to time)

See [docs] (https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/) for resources/in-depth description of how to write Kubernetes resource objects

See also [here] (https://crontab.guru/) for a simple/visual representation of how to schedule a cron job

- Since our workloads are namespaced, some of these commands might require a `-n <NAMESPACE>`. For example `kubectl get jobs -n <NAMESPACE> --watch`
- **Deployments** provide declarative updates for pods and ReplicaSets. ReplicaSets creates pods in the bg.
- **Cron Job** creates a job that runs on a repeating schedule. Useful for creating periodic and recurring tasks

## Troubleshooting

- `kubectl describe pods <my-pod-name>` will display details on the pod created for that job
- `kubectl logs -n <NAMESPACE> <NAME>`
- `kubectl config use-context my-cluster-name`

## QUESTIONS

- What are EC2 instances
- What is autoscaling

`app: irwin` vs `job: integrations` - integrations are more for components, while irwin are more for anything in irwin-api
