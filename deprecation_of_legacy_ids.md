# Deprecation of Legacy Ids

## Goals
- Activity id should be a uuid
- Contacts/Institutions should be a uuid
- Tags should be a uuid

## Strategy
Anywhere that a table was referencing the activity, (ie: had `activity_id` in their table) had be saving the activity `common_id`. At this point Activity already had a auto-generated `common_id` column.

The tables that had this relationship were
- Internal Attendees
- Activity Securities
- Attendees
- Taggings (as `taggable_id`)

Taggings was to be done last together with the final Activity id swap since it was a polymorphic relationship (with Contacts/Institutions)

## Steps
### Activity Foreign Key Relationship - Attendees
The strategy for each of the relationships was similar, but `attendees` was a little more self involved.

1. FE should start send back the `activity_common_id` in all post payloads for saving an attendee
2. Add the `activity_common_id` column to the `attendees` table
3. BE should begin accepting and saving the `activity_common_id` when saving an attendee
4. BE migration to back populate the `activity_common_id`
5. Attendees is unique in that it generates a uuid in the FE for the `activity_id` as we intermittently save the activity. So the payload to save an attendee already includes the `activity_common_id`.
6. Once we are confident that we are always saving the `activity_common_id`, we can then rename the `activity_id` column to `activity_legacy_id` and the `activity_common_id` to be `activity_id` and `activity_id` can now be not null. (**20200414142133_update_attendees_legacy_ids_to_common**)
7. Ensure that the relationship between the attendee and an activity is updated as well as any places there are references of `activities.id` = `attendees.activity_id` to `activities.common_id` = `attendees.activity_id`
8. Make sure that `activity_common_id` is still being serialized since it is still used in the FE.
9. FE should no longer need to send `activity_common_id` and the BE no longer needs to save and serialize it.

__NOTE:__ I also added a `person_id` and `organization_id` to the attendee payload for the eventual switch of contact/institutions to use the `common_id`

### Activity
Once all the relationships have been switched, we can now swap the Activity id from a legacy to common id and the taggings `taggable_id` for activities to the `taggable_common_id`

1. Migration to swap Activity PK for common id and `taggings.taggable_id` to `activities.id`
2. Ensure all FK's relationships point to `activties.id` and not `activities.common_id`

### Contacts/Institutions
Change the contact/institution views to use a common id and simultaneously update the taggings taggable_id. Since they had a polymorphic relationship with taggings, this had to be removed first. So did some side work to remove the poly relationship by accepting nested attributes for taggings. This required allowing to save by both how we did currently (ie: using tag_ids) and by nested atributes. (**20200520143544_taggings_use_common_id**)

1. FE Ensure that `taggable_common_id` was always being passed as part of the payload on a tagging
2. BE always save `taggable_common_id` and backpopulate any empty ones
3. BE Migration - Since nothing was using the Contact/Institution legacy id in the system, we could safely just transition the Contact/Institution id to a common id, while simultaneously changing the `taggable_id` for Contacts/Institutions
4. This ended up just working because then new entities were just saved with this new common id

### Tags
The swap of the `tags.id` from a legacy to a uuid. This goes in hand with also swapping the `taggings.tag_id` to use the same common id as well.

1. FE ensure that anywhere we're comparing `tags.id` === `taggings.tag_id` is using (`tags.id` === `taggings.tag_id` || `tags.id` === `taggings.tag_common_id`). After we swap the tag id to be a common id, the FE will still contain a cache (due to the context) that would still have the tag id as a legacy id.
2. BE migration - rename `common_id` to `id` and `id` to `legacy_id` for tags. Fix up the relationships and replace use of `tag.common_id` with `tag.id`

### Taggings
Finally, the swap of the `taggings.tag_id` to be the `tag.id`. This should be as simple as the migration over and the deprecation of the `tag_common_id`
