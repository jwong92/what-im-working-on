# Basics of writing a Factset Component
At a very high level, the purpose behind these components, is very literally to import data from our factset (FS) database through event sourcing, and then consuming these events to create CRUD commands that would eventually make their way into the irwin database to then be used to create our api's for our various applications.

Each component would ideally represent a specific domain of our business. For example, we have People, Organizations, and Securities, which are represented by our components `person_component`, `organization_component` and `security_component` respectfully. These domains should ideally be very well understood by the product, business and development team, as once they are written, the idea is that they should not be changing too much after.

Factset data can be accessed via our `fds_user`, please ask someone with the power to grant access if it is not accessable.

## Creating the Component
There are two general ways we can go about generating a new component
1. See [here](http://docs.eventide-project.org/user-guide/code-generator.html#installation) - Eventide themseleves have a component code generator, which is a great starting point for developing a component. Usage is simply a matter of installation `gem install evt-command_line-component_generator` and generation `evt component something_component`

2. The second option, arguably the perferred method, is to copy from our [`template_component`](https://gitlab.com/the-platform-group/factset/template_component), and change the word `template` anywhere in the component to the name of the component that you are intending to create. This template component should already come with much of the configurations and settings that we'd want to have in our component.

Upon creating the component, there are a couple of to-do items that must be done ahead of time to get our component in a ready state.
1. Set up your database - run the `db-setup.sh` file in the component. This essentially creates a copy of your `message_store_postgres.json` file, then creates the `message_store` database, and alters the role and password. Upon completion, ensure that your settings file is properly configured to what you want.

2. Install your gems - run the `install-gems.sh` file in the component. This will do a fresh install of your gems specified in your Gemfile. At this point, you should be all set up and ready to continue testing the component.

We typically export our components with the name `factset_x_component`, but use `x_component` within the component itself for less verbosity.

## Order of Operation
Everyone might have a different preference for how they go about writing the component, but this is just my preference:

1. _Define the Entity_ - The name of the entity should be singular, and match that of the component name. The schema of the entity should (in most scenarios) match exactly what factset has determined as their schema. We don't rename any of the attributes at this level, because we can do that downstream. Recall that we are essentially copying data from factset. The primary key should also match that what FS has defined theirs as. Note here that the predicates are also defined, which are used in the loader to determine which events should be written. I typically create the control and testing for the predicates at the same time during this step. See [here](http://docs.eventide-project.org/user-guide/entities.html) for more information.

2. _Create Component Events_ - Depending on how you created the component, the `messages` folder may or may not exist. Since these components are typically only consumed, we do not usually include `command` files. Events that are typically included are: `initiated`, `changed`, `identified` and `deleted`. I create the control for these at the same time. See [here](http://docs.eventide-project.org/user-guide/messages-and-message-data/messages.html) for more information. Only attributes that are **necessary** for defining the event should be included.

3. _Project Events onto the Entity_ - This is very literally copying (projecting) the attributes from the event onto the entity. Key things to note are that message/event attributes are literally formatted to JSON when they're stored in the message store. However, when we are projecting these attributes onto the entity, some attributes will need to be converted (ie: a date string from an event will need to parsed to a date object). I also typically write projection tests for this step as well.

4. _Identify any FS Entities/Securities_ - Sometimes, a component that is being imported from factset will have attributes to other FS identifiers. These typically end with `x_id`. Common ones are `factset_entity_id` and `fsym_id`, where `factset_entity_id` values are 6 alphanumeric characters followed by a dash `E` (ie: `04BRLT-E`), and `fsym_id` are also 6 alphnumeric characters followed by a dash `S` (ie: `VNN3BR-S`). Since we do not want to use factset identifiers as our own identifiers (for a host of different reasons), we have a way of identifying these FS identifiers and mapping them to our own `uuid`'s that are ultimately how we identify these records in irwin.

- An __factset entity__ (**not** to be confused with our component entity) is typically denoted with a `-E` at the end, while a factset security is typically denoted with a `-S` at the end.

- To aid with identification of either a factset entity or security, we have created the [`entity_identification_component`](https://gitlab.com/the-platform-group/factset/entity_identification) to identify ALL factset entities, and use the [SecurityComponent::Identify](https://gitlab.com/the-platform-group/factset/security_component/-/blob/master/lib/security_component/identify.rb) command
to identify ALL factset securities. Both ensure that we do not associate 1 FS entity to two different irwin identities, as that would be very bad. Entity/Security identification can be found under the loader folder. Within it, the `identify` file is where the identification of these entities occurs.

- The important thing about FS entity identification too, is that even if the FS entity itself has not yet been imported, but we are importing a record that references the FS entity, we can assign our unique `uuid` to this FS identifier first, and when it comes time to import the FS entity itself, we will already have mapped that FS entity to our unique `uuid`. There is no need to "wait" for a specific component to identify the FS entity first. For example:

- the `filing_history_13f_component` has attribute `factset_entity_id`, which is a reference to the entity of a 13f holding. If for example, we import a row using the `filing_history_13f_component`, and the `factset_entity_id` has not yet been identified, the `filing_history_13f_component` can identify this FS entity first (ie: map the `factset_entity_id` to a `uuid`), and go on its merry way. Then, whenever we end up importing this `factset_entity_id`, (using the `holding_13f_component`), since this record has already been identified by the `filing_history_13f_component`, we just need to find the associated `uuid`, and then everything is aligned.

- **NOTE** that this identification of other entities (under the loader folder) should not be confused with the top level file, which is used for the identification of itself. See more information below.

- Each component should have a top level `identify` file. As mentioned, this is for identification of itself. The underlying `id` for a component could be a FS identifier (as explained above), in which case similar to the above, we would identify the `id` of the component using the `entity_identification_component` or `SecurityComponent::Identify`. However, there are cases in which a component's underlying `id` could be a composite of a number of different attributes (see any holding component for an example). In these cases, there is no specific entity that this `id` is referencing, and so we instead use the [`identification`](git@gitlab.com:the-platform-group/factset/organization_component.git) component to simply generate a unique `uuid` for each composite `id`.

5. _Writing the Query_ - This is simply a SQL query that selects the attributes in questions from the FS table of interest. We typically do not alter the column names, and import them exactly as they are called in FS. We just want to keep things as close as possible, so things are easier to compare. If names need to be changed, they can be done so downstream. The attributes selected should match the name definitions in the entity schema. Some components are a little more nuanced (see any holding components), where we have date conditions, and references to the import tracking tables, but others can be as simple as selecting all attributes from a table. Before writing the query, it might be a good idea to consult with a couple team members to ensure that we are capturing everything we want.

6. _Writing the loader_ - This is what I would consider one of the last steps. The loader is where all of the above pieces come together. At a high level, the loader opens a connection to the fds database, and runs the query against it. For each row returned, it will determine, using the predicates that have been defined in our entity, which message/event it will write to the message store. Something to note here, similar to our projections, is that attributes coming from our query, will be in various datatypes, but when being written to the message_store, it will be in json. So, any unsupported data types need to be converted. This should also be very well tested as well.

7. _Telemetry and Logger Info_ - At this point, I'd then add in some telemetry and log information so that we can see its' progress when the component is being run

8. _Script_ - This is the very final step (yay!). We typically have a cron job for each component that periodically calls this script. In this script, we call the loader, and then output the results in a slack channel. We also connect the component to rollbar, so that we can see any issues arise there as well. Note that we have started to include an environment variable called `NOTIFY_SLACK`, which can be found in the `filing_history_13f_component` script, that allows us to toggle the slack notifications, as after a component has been running smoothly for some time, we've realized it no longer needs to continuously update the slack channel, as it congests it from trying to see outputs we actually care about when we're testing.

## Testing
After the component has been finished, it is typical that we'd manually test it locally to make sure that everything is running smoothly. Typically, I'd insert a new record into the fds table of interest, (making **extra** sure that the values don't conflict with an existing record, and that I am keeping track of it), then tweak the query a bit to select that record, run the script, and psql into my local `message_store` to make sure the record has identified all the necesary entities (including itself), and written an initiated event. Then, I'd update the record, and run the loader, and expect a changed event written in the `message_store` etc... If anything was modified during testing, ensure everything is back to the original condition after finishing.

## Some Q&A
Sample Error:
```
{:error=>#<Sequel::DatabaseDisconnectError: PG::ConnectionBad: could not translate host name "postgres" to address: nodename nor servname provided, or not known
Settings cannot be read from settings/message_store_postgres.json. The file doesn't exist. (Settings::Error)
```
Solution: Make sure you have your message_store.json file, and it is properly configured.

Sample Error:
```
uninitialized constant Holding13fComponent::Projection::Deleted (NameError)
```
Solution: Make sure you're requiring your files after they've been created, or requiring any gems that are installed

Sample Error:
```
undefined method `entity_name' for Holding13fComponent::Projection:Class (NoMethodError)
```
Solution: Make sure to include the right modules from eventide

`Q: What is the purpose of change_id and deletion_id in the events?`

These are reservation ID's for idempotence. Unlike the message ID's you see in the message store, these reservation ID's are baked into the business logic of a messaging system. Although they are not currently used (or maybe they are when you are reading this), they will be used for future cross-service communication. Eventually, we'll be consuming theses messages and sending them to other serivces through commands. Events that would have these reservation ID's are only on those that could happen multiple times. For example, changed, deleted, re_initied. Events like initiated or identified are themsleves idempotent, and therefore do not require a reservation ID. Refer to this document [here](https://docs.google.com/document/d/1pUPCt5hnzYnjrjtyUjBzlMIOBCxjYBUndhyP2WGaeCI/edit#heading=h.n4wxk2pr7hx1) for more information.

## Example Data Structure for a FS Table

We consume attributes from this FS table in what we call our `filing_history_fund_component`
```
 \d+ fds.own_v5_own_ent_fund_filing_hist
                                    Table "fds.own_v5_own_ent_fund_filing_hist"
      Column       |         Type          | Collation | Nullable | Default | Storage  | Stats target | Description
-------------------+-----------------------+-----------+----------+---------+----------+--------------+-------------
 factset_fund_id   | character(8)          | C         | not null |         | extended |              |
 form_type         | character varying(10) | C         | not null |         | extended |              |
 report_date       | date                  |           | not null |         | plain    |              |
 transfer_date     | date                  |           | not null |         | plain    |              |
 filing_date       | date                  |           | not null |         | plain    |              |
 total_reported_mv | double precision      |           |          |         | plain    |              |
Indexes:
    "own_v5_own_ent_fund_filing_hist_pkey" PRIMARY KEY, btree (factset_fund_id, form_type, report_date, transfer_date, filing_date)
```

We consume attributes from this FS table in what we call our `organization_component`
```
\d+ fds.own_v5_own_ent_coverage
                                    Table "fds.own_v5_own_ent_coverage"
      Column       |     Type     | Collation | Nullable | Default | Storage  | Stats target | Description
-------------------+--------------+-----------+----------+---------+----------+--------------+-------------
 factset_entity_id | character(8) | C         | not null |         | extended |              |
 is_issuer         | smallint     |           | not null |         | plain    |              |
 is_fund           | smallint     |           | not null |         | plain    |              |
 is_inst           | smallint     |           | not null |         | plain    |              |
 is_stakeholder    | smallint     |           | not null |         | plain    |              |
 is_uksr_holder    | smallint     |           | not null |         | plain    |              |
 is_uksr_bo_holder | smallint     |           | not null |         | plain    |              |
 is_contact        | smallint     |           | not null |         | plain    |              |
Indexes:
    "own_v5_own_ent_coverage_pkey" PRIMARY KEY, btree (factset_entity_id)
```

## Closing Remarks
This is a very brief overview of how I would go about writing a component, but it is by no means the _only_ way. Some things that I have left out include substitutes, controls and store. I've also left out a lot of mechanical stuff that eventide helpers provide, so I would supplement this by going through the [eventide docs](http://docs.eventide-project.org/).

Finally, I've also excluded how to actually begin consuming these messages and writing them into irwin. But that can come later :)

Thanks for reading!