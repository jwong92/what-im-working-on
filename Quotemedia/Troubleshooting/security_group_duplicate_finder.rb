#!/usr/bin/env ruby
require 'pry'
require 'json'
require 'logger'

logger = Logger.new(STDOUT)

all_symbols = []
duplicates = []
files = ARGV

logger.info  "#{files.count} files provided"

files.each do |file|
  File.open(file) do |f|
    f.each do |response|
      parsed_response = JSON.parse(response)

      securities = parsed_response.dig('results', 'security')
      symbolstrings = securities.map { |security| security['symbolstring'] }

      all_symbols.concat(symbolstrings)
    end
  end
end

logger.info "#{all_symbols.count} symbolstrings accumulated"

duplicates.concat(all_symbols.group_by{ |e| e }.select { |k, v| v.size > 1 }.map(&:first))

logger.info "#{duplicates.count} duplicates found"

duplicates = 'No duplicates' if duplicates.compact.empty?

puts duplicates