#!/usr/bin/env ruby
require 'pry'
require 'json'

duplicates = []
json_file = ARGV[0]

File.open(json_file) do |file|
  file.each do |response|
    parsed_response = JSON.parse(response)
    securities = parsed_response.dig('results', 'security')

    symbolstrings = securities.map { |security| security['symbolstring'] }

    duplicates << symbolstrings.detect { |symbol| symbolstrings.count(symbol) > 1 }

    duplicates = 'No duplicates' if duplicates.compact.empty?
  end
end

puts duplicates