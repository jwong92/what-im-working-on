# Quotemedia API Research

## QM API List
- getExchangeHistory ([docs](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020865-xml-json-getexchangehistory))
- getFullHistory ([docs](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020867-xml-json-getfullhistory))
- getSecurityMaster ([docs](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000021268-xml-json-getsecuritymaster))

### getExchangeHistory
- The getExchangeHistory call provides end of day data dumps filtered by exchanges. This tool will dump end of day data for all symbols in a specified exchange that had a trade occur on a particular date stamp, i.e., data will not return for symbols that did not have a trade occur.
- Please note that XML/JSON requests for an entire exchange tend to return file sizes too large to be processed by internet browsers.

- Some params to include/details

|parameter|required|default|value|description|
|---------|--------|-------|-----|-----------|
|**exgroup**|yes (or excode)|none|text|find exgroup code here [docs](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020527-exchange-code-group-list)|
|**excode**|yes (or exgroup)|none|text|find excode code here [docs](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020527-exchange-code-group-list)|
|**webmasterId**|yes|none|numberic|unique to our company|
|**date**|no|most recent trading day|YYYY-MM-DD|Date of historic data required|

Example CURL:
- Done on 2020-09-11, and since date was not specified returns data for 2020-09-10
`curl -H "Authorization: Bearer XXXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=tsx&webmasterId=XXXXXX" | json_pp`

```
{
   "results" : {
      "copyright" : "Copyright (c) 2020 QuoteMedia, Inc.",
      "history" : [
         {
            "key" : {
               "symbol" : "AAB:CA",
               "exchange" : "TSX",
               "exShName" : "TSX",
               "exLgName" : "Toronto Stock Exchange"
            },
            "equityinfo" : {
               "longname" : "Aberdeen International Inc.",
               "shortname" : "Aberdeen Interna",
               "optionable" : "false"
            },
            "datatype" : "Equity",
            "adjusted" : true,
            "unadjusted" : false,
            "symbolstring" : "AAB:CA",
            "fundamental" : {
               "totalsharesoutstanding" : 112052282,
               "shareclasslevelsharesoutstanding" : 112052282
            },
            "eoddata" : [
               {
                  "low" : 0.05,
                  "date" : "2020-09-10",
                  "high" : 0.05,
                  "sharevolume" : 1000,
                  "open" : 0.05,
                  "vwap" : 0.05,
                  "close" : 0.05
               }
            ]
         },
         {
            "adjusted" : true,
            "unadjusted" : false,
            "equityinfo" : {
               "optionable" : "true",
               "shortname" : "Advantage Oil & ",
               "longname" : "Advantage Oil & Gas Ltd."
            },
            "datatype" : "Equity",
            "key" : {
               "exchange" : "TSX",
               "symbol" : "AAV:CA",
               "exShName" : "TSX",
               "exLgName" : "Toronto Stock Exchange"
            },
            "fundamental" : {
               "totalsharesoutstanding" : 188112797,
               "shareclasslevelsharesoutstanding" : 187575344
            },
            "symbolstring" : "AAV:CA",
            "eoddata" : [
               {
                  "vwap" : 1.7921,
                  "open" : 1.85,
                  "close" : 1.79,
                  "date" : "2020-09-10",
                  "low" : 1.76,
                  "sharevolume" : 1436846,
                  "high" : 1.87
               }
            ]
         },
         {
            "key" : {
               "exShName" : "TSX",
               "symbol" : "ABT:CA",
               "exchange" : "TSX",
               "exLgName" : "Toronto Stock Exchange"
            },
            "equityinfo" : {
               "longname" : "Absolute Software Corporation",
               "shortname" : "Absolute Softwar",
               "optionable" : "false"
            },
            "datatype" : "Equity",
            "adjusted" : true,
            "unadjusted" : false,
            "eoddata" : [
               {
                  "close" : 13.44,
                  "vwap" : 13.6833,
                  "open" : 14.21,
                  "sharevolume" : 206184,
                  "high" : 14.29,
                  "date" : "2020-09-10",
                  "low" : 13.36
               }
            ],
            "symbolstring" : "ABT:CA",
            "fundamental" : {
               "shareclasslevelsharesoutstanding" : 42566003,
               "totalsharesoutstanding" : 42474553
            }
         },
         {
            "eoddata" : [
               {
                  "close" : 39.7,
                  "vwap" : 39.9324,
                  "open" : 40.5,
                  "sharevolume" : 4496381,
                  "high" : 40.54,
                  "date" : "2020-09-10",
                  "low" : 39.53
               }
            ],
            "fundamental" : {
               "shareclasslevelsharesoutstanding" : 1778068071,
               "totalsharesoutstanding" : 1778068071
            },
            "symbolstring" : "ABX:CA",
            "adjusted" : true,
            "unadjusted" : false,
            "equityinfo" : {
               "longname" : "Barrick Gold Corporation",
               "shortname" : "Barrick Gold Cor",
               "optionable" : "true"
            },
            "datatype" : "Equity",
            "key" : {
               "exLgName" : "Toronto Stock Exchange",
               "symbol" : "ABX:CA",
               "exchange" : "TSX",
               "exShName" : "TSX"
            }
         },
         {
            "symbolstring" : "AC:CA",
            "fundamental" : {
               "totalsharesoutstanding" : 296353172,
               "shareclasslevelsharesoutstanding" : 296703625
            },
            "eoddata" : [
               {
                  "low" : 17.78,
                  "date" : "2020-09-10",
                  "high" : 18.435,
                  "sharevolume" : 4522536,
                  "open" : 17.99,
                  "vwap" : 18.0981,
                  "close" : 17.81
               }
            ],
            "key" : {
               "exLgName" : "Toronto Stock Exchange",
               "exShName" : "TSX",
               "symbol" : "AC:CA",
               "exchange" : "TSX"
            },
            "unadjusted" : false,
            "adjusted" : true,
            "equityinfo" : {
               "shortname" : "Air Canada",
               "longname" : "Air Canada Voting and Variable Voting Shares",
               "optionable" : "true"
            },
            "datatype" : "Equity"
         },
         {
            "eoddata" : [
               {
                  "open" : 0.16,
                  "vwap" : 0.15822,
                  "close" : 0.12,
                  "date" : "2020-09-10",
                  "low" : 0.12,
                  "high" : 0.18,
                  "sharevolume" : 61005
               }
            ],
            "fundamental" : {
               "shareclasslevelsharesoutstanding" : 25000000
            },
            "symbolstring" : "ACB.WT:CA",
            "unadjusted" : false,
            "adjusted" : true,
            "datatype" : "Equity",
            "equityinfo" : {
               "optionable" : "false",
               "shortname" : "Aurora Cannabis ",
               "longname" : "Aurora Cannabis Inc. purchase warrants"
            },
            "key" : {
               "exShName" : "TSX",
               "exchange" : "TSX",
               "symbol" : "ACB.WT:CA",
               "exLgName" : "Toronto Stock Exchange"
            }
         },
         {
            "eoddata" : [
               {
                  "close" : 9.76,
                  "open" : 10.09,
                  "vwap" : 9.9955,
                  "high" : 10.31,
                  "sharevolume" : 1957819,
                  "date" : "2020-09-10",
                  "low" : 9.76
               }
            ],
```

### getFullHistory
- The getFullHistory call provides data points for any symbol between a set of dates with all data resulting in one page in date descending order. The results are returned in one full dump rather than the page results offered in our XML/JSON History call.
- Prioritize a subset of securities(?)
- We want to store this information in history? Alternatively we could initially just allow the user to hit the endpoint themselves while we work on a more permanent solution.

- Some params to include/details

|parameter|required|default|value|description|
|---------|--------|-------|-----|-----------|
|**symbol**|yes |none|text|find exgroup code [here](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020471-quotemedia-symbology-document)|
|**webmasterId**|yes|none|numberic|unique to our company|
|**start**|no|one month previous|YYYY-MM-DD|find excode code [here](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020527-exchange-code-group-list)|
|**date**|no|most recent trading day|YYYY-MM-DD|Date of historic data required|

- To note about the return values:
Some output variables are dependent on the input parameters and may not show


Note: QuoteMedia strongly recommends that clients parse the XML/JSON output using the "symbolstring" field as it is guaranteed to be the key identifier that will remain constant as a parameter and was designed for the purposes of matching response output to input. The "symbol" field can and will likely change from time to time and is, therefore, not recommended as a field to reference other data against.

Example CURL:
`curl -H "Authorization: Bearer XXXXX" "http://app.quotemedia.com/data/getFullHistory.json?symbol=MSFT&start=2019-03-21&end=2020-03-23&webmasterId=XXXXXX" | json_pp`

```
{
   "results" : {
      "history" : [
         {
            "symbolstring" : "MSFT",
            "end" : "2020-03-23",
            "key" : {
               "exchange" : "NGS",
               "symbol" : "MSFT",
               "exLgName" : "Nasdaq Global Select",
               "exShName" : "NGS"
            },
            "start" : "2019-03-21",
            "eoddata" : [
               {
                  "avolume" : 78975176,
                  "changepercent" : -0.997,
                  "sharevolume" : 78975176,
                  "open" : 136.3001,
                  "low" : 131.8334,
                  "change" : -1.3629,
                  "close" : 135.2754,
                  "high" : 139.8417,
                  "totaltrades" : 699928,
                  "totalvalue" : 10778577400,
                  "vwap" : 135.6351,
                  "date" : "2020-03-23"
               },
               {
                  "change" : -5.3322,
                  "low" : 135.1561,
                  "open" : 145.2435,
                  "sharevolume" : 84866215,
                  "avolume" : 84866215,
                  "changepercent" : -3.756,
                  "date" : "2020-03-20",
                  "vwap" : 141.3575,
                  "totalvalue" : 12056844900,
                  "totaltrades" : 711979,
                  "high" : 146.3378,
                  "close" : 136.6383
               },
               {
                  "date" : "2020-03-19",
                  "vwap" : 144.1982,
                  "totalvalue" : 12447748900,
                  "totaltrades" : 709147,
                  "high" : 149.372,
                  "close" : 141.9706,
                  "change" : 2.298,
                  "low" : 138.2798,
                  "open" : 142.0303,
                  "sharevolume" : 85922661,
                  "avolume" : 85922661,
                  "changepercent" : 1.645
               },
               {
                  "totalvalue" : 11415852900,
                  "totaltrades" : 755209,
                  "close" : 139.6725,
                  "high" : 145.2435,
                  "date" : "2020-03-18",
                  "vwap" : 139.2182,
                  "open" : 137.285,
                  "sharevolume" : 81593173,
                  "avolume" : 81593173,
                  "changepercent" : -4.21,
                  "change" : -6.138,
                  "low" : 134.3204
               },
               {
                  "changepercent" : 8.234,
                  "avolume" : 81059817,
                  "sharevolume" : 81059817,
                  "open" : 139.2746,
                  "low" : 134.3005,
                  "change" : 11.0922,
                  "high" : 146.7356,
                  "close" : 145.8106,
                  "totaltrades" : 736576,
                  "totalvalue" : 11615565100,
                  "vwap" : 142.4638,
                  "date" : "2020-03-17"
               },
...
         }]}}
```

### getSecurityMaster
- Want to import all quotemedia with open figi

Example CURL:
`curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getSecurityMaster.json?exgroup=tsx&identifier=true&resultsPerPage=1&webmasterId=XXXXXX" | json_pp`

```
{
   "results" : {
      "copyright" : "Copyright (c) 2020 QuoteMedia, Inc.",
      "symbolcount" : 1,
      "nextpage" : "http://app.quotemedia.com/data/getSecurityMaster.json?webmasterId=XXXXXX&page=2&exgroup=TSX&resultsPerPage=1&identifier=true",
      "pagenumber" : 1,
      "security" : [
         {
            "cik" : null,
            "instrumenttype" : "ETF",
            "longname" : "WisdomTree Natural Gas",
            "country" : "CA",
            "exShName" : "TSX",
            "openfigi" : "BBG001SD6WG4",
            "qmidentifier" : "GBI310ZJ60CZ1AW",
            "inception" : "2006-09-22",
            "symbolstring" : "1689:CA",
            "exchange" : {
               "excode" : "TSX",
               "content" : "Toronto Stock Exchange"
            },
            "sectype" : "ETF",
            "exLgName" : "Toronto Stock Exchange"
         }
      ]
   }
}
```

Air Canada
factset_entity_id | 0GKDKD-E
fsym_security_id | WRXC9N-S
fsym_id | WRXC9N-S
openfigi | BBG001SRFYR7

## Notes

**Q: What exactly is the openfigi identifier?** (in this scenario, the openfigi returned by QM is a Share-class level unique global identifier FIGI (Financial Instrument Global Identifier))

A: Twelve character, alpha-numeric identifier. The first 2 characters are upper-case
consonants (including "Y"), the third character is the upper-case "G", characters 4 -11
are any upper-case consonant (including "Y") or integer between 0 and 9, and the last
character is a check-digit. A Share Class level Financial Instrument Global Identifier is
assigned to an instrument that is traded in more than one country. This enables users to
link multiple Composite FIGIs for the same instrument in order to obtain an aggregated
view for that instrument across all countries globally.

Reference [here](https://www.openfigi.com/assets/content/Open_Symbology_Fields-2a61f8aa4d.pdf) and [here](https://www.openfigi.com/)
Visual representation of openfigi [here](https://upload.wikimedia.org/wikipedia/commons/e/e5/Updated_FIGI_tree2.jpg)

## QM Indexes
Ideally, the indexes we'd like access to are:

| Exchange | XCode | XGroup | Access? |
| ------ | ------ | ------- | ------- |
| Morningstar | MSI | MSI | Yes |
| Toronto Stock Exchange | TSX or TSI | TSX| Yes (TSX) No (TSI/TSIED)
| Canadian Venture Exchange | CDX | TSV | Yes
| NYSE Global Index Feed | NYGIF | NYE | Yes
| CME S&P Base Indices  (S&P 500/100 only) | SPIB | CME | No (CME/SPIB)
| Commodity Exchange | CMX | COMEX | Yes

#### Notes
**MSI** exgroup is available to us, I'm just not sure if we're currently querying it
- `curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=msi&webmasterId=XXXX&datatype=7" | json_pp`

Sample Output:
```
{
   "results" : {
      "copyright" : "Copyright (c) 2021 QuoteMedia, Inc.",
      "symbolcount" : 2960,
      "history" : [
         {
            "unadjusted" : false,
            "fundamental" : {
               "shareclasslevelsharesoutstanding" : 0,
               "totalsharesoutstanding" : 0
            },
            "symbolstring" : "^M000105",
            "datatype" : "Index",
            "equityinfo" : {
               "shortname" : "^M000105",
               "longname" : "Morningstar Wide Moat Focus PR EUR",
               "optionable" : "false"
            },
            "adjusted" : true,
            "key" : {
               "exShName" : "MSI",
               "exchange" : "MSI",
               "exLgName" : "Morningstar Indexes",
               "symbol" : "^M000105"
            },
            "eoddata" : [
               {
                  "low" : 0,
                  "sharevolume" : 0,
                  "open" : 0,
                  "close" : 4685.37,
                  "high" : 0,
                  "date" : "2021-01-19"
               }
            ]
         }
      ...
]}}
```

**TSX/TSI/TSIED** exgroup is available for TSX, though not for TSI/TSIED. Unsure if TSI is what we should be using though, according to docs [here](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020527-exchange-code-group-list) note that in their status they say `excode=TSI for indices` and `excode=TSIED for end of day indices`
- ``curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=tsx&webmasterId=XXXX&datatype=7" | json_pp``

Sample Output:

```
{
   "results" : {
      "symbolcount" : 173,
      "copyright" : "Copyright (c) 2021 QuoteMedia, Inc.",
      "history" : [
         {
            "symbolstring" : "^15NT:CA",
            "eoddata" : [
               {
                  "open" : 0,
                  "sharevolume" : 0,
                  "low" : 0,
                  "date" : "2021-01-19",
                  "close" : 225.1384,
                  "high" : 0
               }
            ],
            "equityinfo" : {
               "optionable" : "false",
               "shortname" : "^15NT:CA",
               "longname" : "S&P/TSX Composite Index (Net TR)"
            },
            "adjusted" : true,
            "fundamental" : {
               "totalsharesoutstanding" : 0,
               "shareclasslevelsharesoutstanding" : 0
            },
            "datatype" : "Index",
            "unadjusted" : false,
            "key" : {
               "exchange" : "TSIED",
               "exShName" : "TSX",
               "exLgName" : "Toronto Stock Exchange",
               "symbol" : "^15NT:CA"
            }
         },
         ...
}]}
```

**TSV&**
- `curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=tsv&webmasterId=XXXX&datatype=7" | json_pp`
```
{
   "results" : {
      "copyright" : "Copyright (c) 2021 QuoteMedia, Inc.",
      "history" : [
         {
            "equityinfo" : {
               "longname" : "Venture Advance Decline Difference",
               "shortname" : "^ADDIFFVN:CA",
               "optionable" : "false"
            },
            "fundamental" : "",
            "key" : {
               "exchange" : "TSVST",
               "exShName" : "TSVST",
               "exLgName" : "TSXV Market Stats",
               "symbol" : "^ADDIFFVN:CA"
            },
            "eoddata" : [
               {
                  "close" : 148,
                  "high" : 256,
                  "low" : 0,
                  "sharevolume" : 148,
                  "open" : 0,
                  "date" : "2021-01-19"
               }
            ],
            "datatype" : "Index",
            "symbolstring" : "^ADDIFFVN:CA",
            "adjusted" : true,
            "unadjusted" : false
         }
         ...
   }]}
```

**NYE** exgroup is available to us, I'm just not sure if we're currently querying it
- `curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=nye&webmasterId=XXXX&datatype=7" | json_pp`

Sample Output:

```
{
   "results" : {
      "symbolcount" : 25,
      "history" : [
         {
            "adjusted" : true,
            "datatype" : "Index",
            "equityinfo" : {
               "optionable" : "false",
               "shortname" : "^ADDIFFR",
               "longname" : "^ADDIFFR"
            },
            "unadjusted" : false,
            "fundamental" : "",
            "key" : {
               "exLgName" : "NYSE Arca",
               "exShName" : "ARCA",
               "symbol" : "^ADDIFFR",
               "exchange" : "ARCA"
            },
            "eoddata" : [
               {
                  "low" : 0,
                  "high" : 974,
                  "open" : 0,
                  "date" : "2021-01-19",
                  "sharevolume" : 971,
                  "close" : 971
               }
            ],
            "symbolstring" : "^ADDIFFR"
         }
         ...
      ]
   }
}
```

## QM Commodoties
There are actually a lit of commodity exchanges in quotemedia [here](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000020527-exchange-code-group-list). I'm not sure if there are any we are interested in there. Note that we have no access to the `CME` exogroup, so if we want access to any of these commodities (or the CME S&P Base Indices) then we'd need access

**SPMTL** - Spot Market Metals which are under the datatype of `Forex`, though not included as a datatype option to query by. So no datatype is provided.
`curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=SPMTL&webmasterId=XXXX" | json_pp`
Sample Output:

```
{
   "results" : {
      "history" : [
         {
            "adjusted" : true,
            "datatype" : "Forex",
            "unadjusted" : false,
            "symbolstring" : "/SISPUSD",
            "fundamental" : "",
            "eoddata" : [
               {
                  "high" : 25.406,
                  "open" : 24.9945,
                  "sharevolume" : 0,
                  "close" : 25.2325,
                  "date" : "2021-01-19",
                  "low" : 24.977
               }
            ],
            "key" : {
               "symbol" : "/SISPUSD",
               "exLgName" : "Spot Market Metals",
               "exShName" : "SPMTL",
               "exchange" : "SPMTL"
            },
            "equityinfo" : {
               "optionable" : "false",
               "longname" : "Spot Silver - USA Dollars",
               "shortname" : "Spot Silver US"
            }
         }
      ]
   }
}
```

## QM Consolidated Volume
- As of Juky 20, 2021, we now have access to consolidated volume tickers, which quotemedia has more information on [here](https://quotemediasupport.freshdesk.com/support/solutions/articles/13000039341-canadian-consolidated-quotes-ccq-data). These are essentially another ticker for each Canadian company. It is something they provide us that consolidates all the volumes for a security across the various trading venues in Canada. The exgroups we have access to for these consolidated volumne tickers are:

| Exchange | XCode | XGroup | Access? |
| ------ | ------ | ------- | ------- |
| TSX Canadian Consolidated Quotes (CCQ) | TSXC | TSXC | Yes
| TSX Canadian Consolidated Quotes (CCQ) | TSXVC | TSXVC | Yes
| NEO Canadian Consolidated Quotes (CCQ) | AQNC | AQNC | Yes
| CSE Canadian Consolidated Quotes (CCQ) | CNQC | CNQC | Yes

Similar to how to fetch eod pricing for the above, consolidated volume works much the same way

`curl -H "Authorization: Bearer XXXX" "http://app.quotemedia.com/data/getExchangeHistory.json?exgroup=TSXC&webmasterId=XXXX" | json_pp`
