# Holdings Incremental Refresh
The process of taking new changes from the ds1_holdings_13f and inserting them into the new ds1_holding_calcs.thirteenf table. This new table will have all the updates holdings and positions including the calculation of the sell-offs and expiry's.

# Queries

## Step 1
---
## Create the new 13f Holding Calcs Table
- This table will serve as the __updated__ 13f which will be then used in the next step of the unionized view.
- We will use the FS table to pick and choose which of these rows to update, delete and create based on the new positions and holdings that come in (or lack of).

```
CREATE SCHEMA IF NOT EXISTS ds1_holdings_calcs
;

DROP TABLE IF EXISTS ds1_holdings_calcs.thirteen_fs
;

SET work_mem = '256MB'
;

CREATE TABLE ds1_holdings_calcs.thirteen_fs (
    id uuid NOT NULL
  , holder_id uuid NOT NULL
  , filer_id uuid NOT NULL
  , security_id uuid NOT NULL
  , report_date date NOT NULL
  , shares double precision NOT NULL
  -- TODO: this should be NOT NULL in the real world
  , valid_until date
  -- TODO: add an actual interval column here
  , record_source text NOT NULL
  , updated_at timestamp NOT NULL
  -- TODDO: add the source updated_at here as well to see any metrics from ds1 update to this table update
  , source_updated_at timestamp NOT NULL

  , PRIMARY KEY(id)
  , CHECK(record_source IN ('REPORTED', 'INTERNAL_ZEROED_FILING_ABSENCE', 'INTERNAL_ZEROED_EXPIRY_DATE'))
  , UNIQUE(filer_id, report_date, security_id)
)
;
```

## Updating Changed Records and Creating New Ones
- This should **only** `UPSERT` any holdings that are of `source_type` **REPORTED**. This is because anything that isn't, is technically manufactured by US. (ie: a sell-off or expiry)
- Incoming holdings from FS are always going to be of `source_type` **REPORTED**. Anything that has been recently updated in the `ds1_holding_13fs` table, either was deleted or something has changed.
- If we've previously created a internal zeroed holding (ie: a sell-off or an expiry), and a new holding comes in for that report date, we update the `shares`, `updated_at` and `record_source` to what is coming in from FS.

### Testing
1. A __new__ holding that is of type `REPORTED` should be created. IE: would not have existed in ds1_holdings_calcs.thirteen_fs initially, but then would after refresh
2. A __changed__ holding that is of type `REPORTED` should be updated. IE shares, updated_at and record_source will update after refresh
3. A __new/valid__ holding would choose the `adjusted_shares` over that of the `shares`, and update the `updated_at` date to now
4. A __changed/valid__ holding for a previous holding of `source_type` that is **NOT** `REPORTED` should update the `record_source` to `REPORTED`

```
INSERT INTO ds1_holding_calcs.thirteen_fs (
    id
  , holder_id
  , filer_id
  , security_id
  , report_date
  , shares
  , record_source
  , updated_at
  , source_updated_at
)
SELECT
    incoming.id
  , hist.holder_id
  , hist.filer_id
  , incoming.security_id
  , incoming.report_date
  , COALESCE(incoming.adjusted_shares, incoming.shares) AS shares
  , 'REPORTED' AS record_source
  , now() AS updated_at
  , incoming.updated_at AS source_updated_at
FROM ds1_holding_13fs AS incoming
-- this join is just to get the holder_id that the filer_id
-- maps to
INNER JOIN (
  SELECT DISTINCT
      filer_id
    , report_date
    , holder_id
  FROM ds1_filing_history_13fs
) AS hist ON hist.filer_id = incoming.holder_id
  AND hist.report_date = incoming.report_date
WHERE incoming.updated_at >= :updated_at
ON CONFLICT(filer_id, report_date, security_id) DO UPDATE
SET
    shares = EXCLUDED.shares
  , updated_at = now()
  -- switch from 'INTERNAL_ZEROED_FILING_ABSENCE' back to 'REPORTED'
  -- because we could get a new position for the date that the
  -- 'INTERNAL_ZEROED_FILING_ABSENCE' existed at
  , record_source = 'REPORTED'
;

-- 5224.711 ms
-- ~300k records
-- updating every row (running the query again) took a similar amount of time
```

## Update Holder ID <-> Filer ID Mappings
- 13F's (and Funds) are at a holder<->filer level, which is one level more granular than at the holder<->security level.
- From the `ds1_filing_history_13fs`, the `holder_id` may have changed for a `filer_id`.
- For a given filing filed on a given report date for a given `holder_id`, if the `holder_id` no longer matches the one in our `ds1_holding_calcs.thirteen_fs` table, then we must update it to the incoming `holder_id` from the `ds1_filing_history_13fs` table.
- This is also scoped to the `:updated_at`, because we don't care about ones that have not changed

### Testing
1. The existing `holder_id` has **NOT** changed, and **NO** update is made to the `holder_id` or `updated_at` timestamp
2. The existing `holder_id` **HAS** changed, and an update **IS** made to the `holder_id` or `updated_at` timestamp

```
SET work_mem = '256MB'
;

-- we need to handle the filer ID <-> holder ID updates separately, since
-- that mapping comes from a different table (ds1_filing_history_13fs)
UPDATE ds1_holding_calcs.thirteen_fs AS existing
SET
    holder_id = filing_hist.holder_id
  , updated_at = now()
FROM (
  SELECT DISTINCT
      filer_id
    , report_date
    , holder_id
  FROM ds1_filing_history_13fs
  WHERE updated_at >= :updated_at
) AS filing_hist
WHERE existing.filer_id = filing_hist.filer_id
  AND existing.report_date = filing_hist.report_date
  -- scope to filers whose holders have actually changed,
  -- so we don't update a bunch of rows unnecessarily
  AND existing.holder_id <> filing_hist.holder_id
;
-- 2595.095 ms
-- 0 row(s) updated. worth keeping an eye on this.
```

## Delete any Holdings that no longer exist
- We delete any holdings that no longer exist. It is fine to use the PK (id) here, because these are comparing any deletions from FS to irwin, which means we will have event sourced that id, and it isn't a record created by us.
- In addition, we delete any holdings from the latest report date table, because we need a way to be able to capture what the next latest date the reported holding is if the holding being deleted is the latest. (This will make more sense later)

```
WITH deleted_holdings AS (
  DELETE FROM ds1_holding_calcs.thirteen_fs AS existing
  WHERE EXISTS (
    SELECT *
    FROM ds1_holding_13fs AS incoming
    WHERE incoming.deleted_at >= :deleted_at
      AND incoming.id = existing.id
  )
  RETURNING id
), deleted_latest_report_dates AS (
  DELETE FROM ds1_holding_calcs.latest_thirteen_f_report_dates AS latest_table
  WHERE EXISTS (
    SELECT *
    FROM deleted_holdings AS just_deleted
    WHERE just_deleted.id = latest_table.id
  )
  RETURNING *
)

INSERT INTO ds1_holding_calcs.latest_thirteen_f_report_dates (
    id
  , filer_id
  , security_id
  , latest_report_date
  , updated_at
)
SELECT
    existing.id
  , latest_report_dates.filer_id
  , latest_report_dates.security_id
  , latest_report_dates.latest_report_date
  , latest_report_dates.updated_at
FROM (
  SELECT
      just_deleted.filer_id
    , just_deleted.security_id
    , MAX(existing.report_date) AS latest_report_date
    , now() AS updated_at
  FROM deleted_latest_report_dates AS just_deleted
  INNER JOIN ds1_holding_calcs.thirteen_fs AS existing ON existing.filer_id = just_deleted.filer_id
    AND existing.security_id = just_deleted.security_id
  GROUP BY 1, 2
) AS latest_report_dates
INNER JOIN ds1_holding_calcs.thirteen_fs AS existing ON existing.filer_id = latest_report_dates.filer_id
  AND existing.security_id = latest_report_dates.security_id
  AND existing.report_date = latest_report_dates.latest_report_date
WHERE existing.record_source = 'REPORTED'
;
```

## Create new filing absence records
- This is essentially determining when a position has been sold off. That is, when we create a new record for an `INTERNAL_ZEROED_FILING_ABSENCE`.
- This happens when a holding that has previously been filed is no longer available in the incoming filing.
- We specify a sell of when the filing date is __between__ (__NOT__ equal) the dates of when the incoming holding was reported and the next valid report date (ie: the valid until), because those (incoming/next valid) **ARE** the report dates, and so if a filing date doesn't actually equal either of them, then they must not have been reported, and are thus considered a sell off.

### Testing
1. A __new__ filing is created, whose report date is **more recent** than the holding report date. The expectation is that we **WILL** create a sell-off record whose report date will be as of the new filing date. To note that date should also be less than the next report date or valid until date.
2. A __new__ filing is created, whose report date is **older** than the holding report date. The expectation is that we would **NOT** create a sell-off, as there is a filing of a holding that exists on that date.
3. A __new__ filing is created whose report date is newer than the expiry of the holding. The expectation is that we would **NOT** create a sell-off, as we have a filing of a holding that exists on the next holding date.
4. We should be checking that when a sell-off **IS** created, that the new record has shares that are 0, record_source is `INTERNAL_ZEROED_FILING_ABSENCE` and the `updated_at` is current.
5. A holding that has multiple filings, should choose the report date of the most recent filing and the includes the security id

```
SET work_mem = '256MB'
;

EXPLAIN ANALYZE
WITH changed_holdings AS (
  -- calculate when changed holdings (holdings that were updated as part of this
  -- refresh run) are valid until
  SELECT
      existing.*
    , COALESCE((
        SELECT report_date
        FROM ds1_holdings_calcs.thirteen_fs AS next_holdings
        WHERE next_holdings.filer_id = existing.filer_id
          AND next_holdings.security_id = existing.security_id
          AND next_holdings.report_date > existing.report_date
        ORDER BY next_holdings.report_date
        LIMIT 1
      ), '9999-12-31') AS next_report_date
  FROM ds1_holdings_calcs.thirteen_fs AS existing
  WHERE existing.updated_at::date = '2021-02-15'
), filer_report_dates AS (
  -- TODO: we might want to store this DISTINCT list somewhere
  -- because we keep recalculating it
  SELECT DISTINCT
      filer_id
    , holder_id
    , report_date
  FROM ds1_filing_history_13fs
)
INSERT INTO ds1_holdings_calcs.thirteen_fs (
    id
  , holder_id
  , filer_id
  , security_id
  , report_date
  , shares
  , record_source
  , updated_at
)
SELECT
    -- generate a deterministic UUID because this just seems nicer...
    uuid_generate_v5(
        '00000000-0000-0000-0000-000000000000'::uuid
      , CONCAT_WS('+', report_dates.filer_id, changed_holdings.security_id, report_dates.report_date)
    ) AS id
  , report_dates.holder_id
  , report_dates.filer_id
  , changed_holdings.security_id
  , report_dates.report_date
  , 0 AS shares
  , 'INTERNAL_ZEROED_FILING_ABSENCE' AS record_source
  , now() AS updated_at
FROM changed_holdings
INNER JOIN filer_report_dates AS report_dates ON report_dates.filer_id = changed_holdings.filer_id
  AND report_dates.report_date > changed_holdings.report_date
  AND report_dates.report_date < changed_holdings.next_report_date
;

-- Handle cases where a filer has sold ALL of its holdings:
-- holder_x files on 2020-12-31, and says it has NO holdings
-- this means we have NO holdings on 2020-12-31 for holder_x in ds1_holding_13fs;
-- the most recent holdings we'll have are from 2020-09-30.
-- we need to calculate 0 holdings (sell-offs) for ALL of the holdings held as at
-- 2020-09-30 for 2020-12-31
```

## Remove expirey dates that are no longer applicable
- The `ds1_holding_calcs.thirteen_fs` table would have had previously calculated sell-off's. This query will delete any records that were previously determined to be a sell-off, but no longer is considered to be one, because a new expiry date was calculated based on an incoming change, or a new position was inserted based on an incoming change.

### Testing
1. Deletes a expiry if `record_source` is `INTERNAL_ZEROED_EXPIRY_DATE`
2. Deletes a expiry if the `updated_at` date is __today__ (ie: the minimum changed report date)
3. Deletes a expiry if the zeroed expiry date is greater than the incoming holding report date
4. Deletes a expiry if the incoming holding report dates expiry date is newer than the existing expiry report date

<!-- Complete this section -->

CASE 1
holder_a | security_a | 2019-05-01 | INTERNAL_ZEROED_EXPIRY_DATE | EXISTING
holder_a | security_a | 2019-06-01 | REPORTED | INCOMING

incoming report date is not older than the existing report date, so it won't ever check the second condition

CASE 2
holder_a | security_a | 2020-01-01 | INTERNAL_ZEROED_EXPIRY_DATE | EXISTING
holder_a | security_a | 2019-04-01 | REPORTED | INCOMING

- incoming report date expires on 2019-12-01, which is older than the existing internal zeroed expiry, so we would not delete that.

```
WITH expiry_interval AS (
  SELECT (quantity || ' ' || unit)::interval AS expiry_interval
  FROM holding_expiry_parameters
  WHERE type = '13f'
)
DELETE FROM ds1_holding_calcs.thirteen_fs AS existing
WHERE existing.record_source = 'INTERNAL_ZEROED_EXPIRY_DATE'
  AND EXISTS (
    SELECT *
    FROM ds1_holding_calcs.thirteen_fs AS changed_recently
    WHERE changed_recently.filer_id = existing.filer_id
      AND changed_recently.security_id = existing.security_id
      AND changed_recently.updated_at >= :updated_at
      -- only REPORTED records can invalidate INTERNAL_ZEROED_EXPIRY_DATE
      -- ones. this would likely be implicitly true (w/out this predicate)
      -- but adding it here for clarity
      AND changed_recently.record_source = 'REPORTED'
      -- scope to newly changed records whose report dates are within
      -- the expiry window
      AND changed_recently.report_date <= existing.report_date
      AND (changed_recently.report_date + (SELECT expiry_interval FROM expiry_interval))::date > existing.report_date
  )
;

```

## Create new expiry dates only for records that expire TODAY
- At this point, the `ds1_holding_calcs.thirteen_f` table would have all new holdings
- We need to check if there are any holdings that expire __today__. These are our manufactured expriy dates, IE they are not sell-offs, but follow that of our documented expiry process (ie: 13f's = 8 months, Stakes = 20 months, Funds = 24 months)
- This step is further divided into 2 queries for better optimization

**PART 1** - Calculate the latest report dates
- This table will calculate the latest report dates of incoming holdings whose report date **IS** of source_type `REPORTED`. The query is set this way, because the latest report date source_type may have been a sell-off (ie: of type `INTERNAL_ZEROED_FILING_ABSENCE`).
- We only expire `REPORTED`, because `INTERNAL_ZEROED_FILING_ABSENCE` and `INTERNAL_ZEROED_EXPIRY_DATE` do not expire.

### Testing
- Of 2 holdings with type `REPORTED`, the report date of the more recent holding is selected
- Of 2 holdings, the most recent filing being type `INTERNAL_ZEROED_FILING_ABSENCE` or `INTERNAL_ZEROED_EXPIRY_DATE`, and the older of type reported, we expect NOT to insert the holding into this table as it has already been expired
1. A holding whose latest report date is __equal__ to 8 months ago should be expired. (IE: record_source = `INTERNAL_ZEROED_EXPIRY_DATE`, 0 shares, and new updated at)
2. A holding whose latest report_date is __before__ (aka older/less than) 8 months ago should be expired. IE, it has already been expired long ago
3. A holding whose latest report_date is __after__ (aka newer/greater than) 8 months ago should **NOT** be expired. IE. it has been reported after the 8 month expiry period.

```
-- what do deleted things mean for this?
EXPLAIN ANALYZE
INSERT INTO ds1_holding_calcs.latest_thirteen_f_report_dates AS EXISTING (
    filer_id
  , security_id
  , latest_report_date
  , updated_at
)
SELECT
    latest_report_dates.filer_id
  , latest_report_dates.security_id
  , latest_report_dates.latest_report_date
  , now() AS updated_at
FROM (
  SELECT
      incoming.filer_id
    , incoming.holder_id
    , incoming.security_id
    , MAX(incoming.report_date) AS latest_report_date
  FROM ds1_holding_calcs.thirteen_fs AS incoming
  WHERE incoming.updated_at::date >= :updated_at
  GROUP BY 1, 2, 3
) AS latest_report_dates
INNER JOIN ds1_holding_calcs.thirteen_fs AS incoming ON incoming.filer_id = latest_report_dates.filer_id
  AND incoming.security_id = latest_report_dates.security_id
  AND incoming.report_date = latest_report_dates.latest_report_date
WHERE incoming.record_source = 'REPORTED'
ON CONFLICT(filer_id, security_id) DO UPDATE
SET
    -- EXCLUDED.latest_report_date = '2019-04-30' checkmark
    -- EXISTING.latest_report_date = '2019-05-01' DELETED

    -- EXCLUDED.latest_report_date = '2020-01-01'
    -- EXISTING.latest_report_date = '2020-03-01' checkmark

    -- EXCLUDED.latest_report_date = '2020-06-01' checkmark
    -- EXISTING.latest_report_date = '2020-05-01'

    -- EXCLUDED.latest_report_date = '2020-06-01' checkmark
    -- EXISTING.latest_report_date = '2020-05-01' DELETED

    -- EXCLUDED.latest_report_date = '2020-06-01'
    -- EXISTING.latest_report_date = '2020-06-01'
    latest_report_date = GREATEST(EXISTING.latest_report_date, EXCLUDED.latest_report_date)
  , updated_at =
      CASE
        WHEN EXCLUDED.latest_report_date > EXISTING.latest_report_date THEN now()
        ELSE EXISTING.updated_at
      END
;

-- filer_x | security_a | 2019-05-01 | REPORTED
-- when we run "today", we do this:
-- get the "latest report date"s:
-- insert into ds1_holdings_calcs.latest_thirteen_f_report_dates:
-- filer_x | security_a | 2019-05-01
-- we create an expiry record:
-- insert into ds1_holdings_calcs.thirteen_fs:
-- filer_x | security_a | 2020-01-01 | INTERNAL_ZEROED_EXPIRY_DATE
-- filer_x | security_a | 2019-05-01 | DELETED
-- filer_x | security_a | 2019-04-30 | REPORTED
-- filer_x | security_a | 2020-12-30 | INTERNAL_ZEROED_EXPIRY_DATE
-- filer_x | security_a | 2020-01-01
-- filer_x | security_a | 2020-02-01
-- filer_x | security_a | 2020-03-01 | LATEST
-- filer_x | security_a | 2020-01-01 | CHANGED: shares increased
-- filer_x | security_a | 2020-02-01
-- filer_x | security_a | 2020-03-01 | LATEST
--13032.402 ms on first (seednig) run
-- REPORTED
-- we don't want to expire something that is:
--  1) younger than 8 months old (i.e. a holding from just yesterday)
--  2) a holding that has ALREADY been expired
--  3) a holding fabricated through our sell-off logic ('INTERNAL_ZEROED_FILING_ABSENCE' record_source)
-- latest report date is 2020-06-14
-- it is a INTERNAL_ZEROED_FILING_ABSENCE
-- handle holdings that expired today
SET work_mem = '256MB'
;
```

**PART 2** - Add expiry date for holdings that expired __today__.
We don't want to expire something that is:
- 1) younger than 8 months old (i.e. a holding from just yesterday)
- 2) a holding that has ALREADY been expired
- 3) a holding fabricated through our sell-off logic ('INTERNAL_ZEROED_FILING_ABSENCE' record_source)
- Using the table from pt. 1 `ds1_holdings_calcs.latest_thirteen_f_report_dates`, we know that all of those holdings have not __ALREADY__ been sold-off or expired, and thus are qualified to potentially be expired.
- Here, we expire based on if the latest report datea
- We know anything from now to 8 months ago (exclusive) hasn't expired yet. Thus, if a report date is newer than 8 months ago, it hasn't expired. On the other hand, if a report date is older than 8 months, it will have expired
- In this case, we only zero if it is equal, because we only care about `TODAY` for now. This will ensure that any holding that expires today will be zeroed, and we won't have to "go back in history" to zero them out.

```
WITH expiry_interval AS (
  SELECT (quantity || ' ' || unit)::interval AS expiry_interval
  FROM holding_expiry_parameters
  WHERE type = '13f'
)
INSERT INTO ds1_holding_calcs.thirteen_fs (
    id
  , holder_id
  , filer_id
  , security_id
  , report_date
  , shares
  , record_source
  , updated_at
  , source_updated_at
)
SELECT
    uuid_generate_v5(
        '00000000-0000-0000-0000-000000000000'::uuid
      , CONCAT_WS('+', latest.filer_id, latest.security_id, now()::date)
    ) AS id
  , filing_history.holder_id
  , latest.filer_id
  , latest.security_id
  , now()::date AS report_date
  , 0 AS shares
  , 'INTERNAL_ZEROED_EXPIRY_DATE' AS record_source
  , now() AS updated_at
  , latest.updated_at AS source_updated_at
FROM ds1_holding_calcs.latest_thirteen_f_report_dates AS latest
INNER JOIN (
  SELECT DISTINCT
      filer_id
    , holder_id
    , report_date
  FROM ds1_filing_history_13fs
) AS filing_history ON filing_history.filer_id = latest.filer_id
  AND filing_history.report_date = latest.latest_report_date
WHERE latest.latest_report_date = (:updated_at::date - (SELECT expiry_interval FROM expiry_interval))::date
;
```

### Create expiry dates for reported holdings that have previously expired (ie: any before today, not inclusive of today)
- Since we captured ones that expired today in the previous query, we do not need to be inclusive of today here
- This is slightly easier, becuase we don't rely on the most recent report date of a holding which we calculated from above. We only care about any previous reported holding of record_source `REPORTED`.

**PART 1** - Select holdings that are eligible for expirey
- We only select holdings that have been updated today, whose report date is older than 8 months ago, and source_type is `REPORTED`.

### Testing
1. Does not insert a holding that is not of type `REPORTED`
2. Does not insert a holding that is not updated after or equal to today
3. Does not insert a holding that is newer than 8 months ago
4. Does insert a holding that is of type `REPORTED`, updated today, and newer than 8 months ago, and has a report date of reported date + 8 months (ie: 8 months later).

```
CREATE TEMPORARY TABLE maybe_expiring_holdings ON COMMIT DROP AS
WITH expiry_interval AS (
  SELECT (quantity || ' ' || unit)::interval AS expiry_interval
  FROM holding_expiry_parameters
  WHERE type = '13f'
)
SELECT
    uuid_generate_v5(
        '00000000-0000-0000-0000-000000000000'::uuid
      , CONCAT_WS('+', filer_id, security_id, (report_date + (SELECT expiry_interval FROM expiry_interval))::date)
    ) AS id
  , holder_id
  , filer_id
  , security_id
  , report_date
  , (report_date + (SELECT expiry_interval FROM expiry_interval))::date AS expiring_report_date
  , 0 AS shares
  , 'INTERNAL_ZEROED_EXPIRY_DATE' AS record_source
  , updated_at
FROM ds1_holding_calcs.thirteen_fs AS expiring
WHERE record_source = 'REPORTED'
  AND updated_at >= :updated_at
  AND report_date < (now() - (SELECT expiry_interval FROM expiry_interval))::date
;

INSERT INTO ds1_holding_calcs.thirteen_fs (
    id
  , holder_id
  , filer_id
  , security_id
  , report_date
  , shares
  , record_source
  , updated_at
  , source_updated_at
)
SELECT
    expiring.id
  , expiring.holder_id
  , expiring.filer_id
  , expiring.security_id
  , expiring.expiring_report_date AS report_date
  , expiring.shares
  , expiring.record_source
  , now() AS updated_at
  , expiring.updated_at AS source_updated_at
FROM maybe_expiring_holdings AS expiring
WHERE NOT EXISTS (
  SELECT *
  FROM ds1_holding_calcs.thirteen_fs AS other_holdings
  WHERE other_holdings.filer_id = expiring.filer_id
    AND other_holdings.security_id = expiring.security_id
    AND other_holdings.report_date > expiring.report_date
    AND other_holdings.report_date <= expiring.expiring_report_date
)
;
```

### Calculate Valid Until Dates
```
EXPLAIN ANALYZE
UPDATE ds1_holdings_calcs.thirteen_fs AS thirteen_fs
SET valid_until = COALESCE((
    SELECT next_record.report_date
    FROM ds1_holdings_calcs.thirteen_fs AS next_record
    WHERE next_record.filer_id = thirteen_fs.filer_id
      AND next_record.security_id = thirteen_fs.security_id
      AND next_record.report_date > thirteen_fs.report_date
    ORDER BY next_record.report_date
    LIMIT 1
  ), '9999-12-31')
FROM ds1_holdings_calcs.minimum_changed_report_dates AS m
WHERE m.holder_id = thirteen_fs.holder_id
  AND m.security_id = thirteen_fs.security_id
  AND thirteen_fs.report_date >= m.minimum_changed_report_date
;
```
- The incoming holding's holder_id is the existing's filer_id

## Step 2
---

### Scope Min Changed Report Dates to the Holder ID
- Calculate the min changed report date for a holder. Since the `min_changed_report_date_thirteen_fs` table scopes to the filing, that means we may have many min_changed_report_dates for each holder (see below):

| holder_id | filer_id | security_id | min_changed_report_date |
| --------- | -------- | ----------- | ----------------------- |
| 12345     | ABC      | XYZ         | 2018-01-01              |
| 12345     | DEF      | XYZ         | 2017-01-01              |

- Since we want to eventually roll-up to the holder, we'll need to choose the `min_changed_report_date` scoped to the holder, we need to be able to choose which of the filings `min_changed_report_date` we want.
- ie: we'll choose filer_id `DEF` because its' report date is the smaller of the two (the true minimum)

```
TRUNCATE TABLE ds1_holding_calcs.holder_min_changed_report_date_thirteen_fs
;

INSERT INTO ds1_holding_calcs.holder_min_changed_report_date_thirteen_fs (
    holder_id
  , security_id
  , min_changed_report_date
)
SELECT
    holder_id
  , security_id
  , MIN(min_changed_report_date) AS min_changed_report_date
FROM ds1_holding_calcs.min_changed_report_date_thirteen_fs
GROUP BY 1, 2
;
```

### Determine all Min Changed Report Dates for each Filer

NEW QUERY
```
EXPLAIN ANALYZE
WITH holders_and_filers AS (
  -- we need to get all of the holder <-> filer mappings for the below lateral
  -- join to work, because we need to scope on filer <-> securities; scoping
  -- on holder <-> securiites would mean we'd pick the holding from an arbitrary
  -- filer, due to the LIMIT 1
  SELECT DISTINCT ON (holder_report_dates.holder_id, security_id, filer_id)
      holder_report_dates.holder_id
    , security_id
    , filer_id
    , holder_report_dates.minimum_changed_report_date
  FROM ds1_holding_calcs.holder_min_changed_report_date_thirteen_fs AS holder_report_dates
  INNER JOIN ds1_filing_history_13fs AS filing_history ON filing_history.holder_id = holder_report_dates.holder_id
  ORDER BY 1, 2, 3, 4
)
SELECT *
FROM holders_and_filers
INNER JOIN LATERAL (
  SELECT
      thirteen_fs.*
    , ABS(thirteen_fs.report_date - holders_and_filers.minimum_changed_report_date) AS days_difference
    , CASE SIGN(thirteen_fs.report_date - holders_and_filers.minimum_changed_report_date)
        WHEN  0 THEN 1 -- prefer holdings on the min changed report date
        WHEN -1 THEN 2 -- then prefer holdings from before
        WHEN  1 THEN 3 -- then prefer holdings from after
      END AS days_difference_sign
  FROM ds1_holding_calcs.thirteen_fs AS thirteen_fs
  WHERE thirteen_fs.filer_id = holders_and_filers.filer_id
    AND thirteen_fs.security_id = holders_and_filers.security_id
  ORDER BY
      days_difference_sign
    , days_difference
  LIMIT 1
) AS minimum_filer_dates ON true
;
```

```
INSERT INTO ds1_holding_calcs.filer_min_changed_report_date_thirteen_fs (
    holder_id
  , filer_id
  , security_id
  , min_changed_report_date
)
SELECT
    thirteen_fs.holder_id
  , thirteen_fs.filer_id
  , thirteen_fs.security_id
  , MAX(thirteen_fs.report_date) AS min_changed_report_date
FROM ds1_holding_calcs.min_changed_report_date_thirteen_fs AS report_date
INNER JOIN ds1_holding_calcs.thirteen_fs AS thirteen_fs ON thirteen_fs.holder_id = report_date.holder_id
  AND thirteen_fs.security_id = report_date.security_id
  AND thirteen_fs.report_date <= report_date.min_changed_report_date
GROUP BY
    thirteen_fs.holder_id
  , thirteen_fs.filer_id
  , thirteen_fs.security_id
;
```

### Calculate validity interval
- For each filer, determine the validity interval

```
INSERT INTO ds1_holding_calcs.thirteen_f_intervals (
  SELECT
      incoming.holder_id
    , incoming.security_id
    , incoming.report_date
    , incoming.shares
    , daterange(
        incoming.report_date
      , incoming.valid_until
      , '[)' -- left brack ( [ ) = inclusive lower bound; right paren ( ) ) = exclusive upper bound
    ) AS validity_interval
  FROM ds1_holding_calcs.thirteen_fs AS incoming
  INNER JOIN ds1_holding_calcs.filer_min_changed_report_date_thirteen_fs AS m ON m.filer_id = incoming.filer_id
    AND m.security_id = incoming.security_id
  WHERE incoming.report_date >= m.min_changed_report_date
)
;
```

### Sum the shares for each holding on a given report date

- `ds1_holding_calcs.thirteen_f_intervals` isn't currently unique, because it has the list of all filings after a given min changed report date.
- For example:

| Filer ID | Holder ID | Security ID | Date	| Shares | Valid Until |
| -------- | --------- | ----------- | ---- | ------ | ----------- |
| FGH	     |   ALPHA	 |    1234     | 2018-01-01 | 400	| -
| FGH	     |   ALPHA	 |    1234	   | 2019-11-30	| 300 |	2020-06-30
| FGH	     |   ALPHA	 |    1234	   | 2020-06-30	| 100 |	9999-12-31
| XYZ	     |   ALPHA	 |    1234	   | 2019-12-31	| 1000 |	2020-03-30
| XYZ	     |   ALPHA	 |    1234	   | 2020-03-30	| 500 |	2020-11-30
| XYZ	     |   ALPHA	 |    1234	   | 2020-11-30	| 0 |	9999-12-31
| IJK	     |   ALPHA	 |    1234	   | 2019-12-31	| 750 |	2020-03-30
| IJK	     |   ALPHA	 |    1234	   | 2020-03-30	| 3000 |	2020-06-30
| IJK	     |   ALPHA	 |    1234	   | 2020-06-30	| 3000 |	2020-09-30
| IJK	     |   ALPHA	 |    1234	   | 2020-09-30	| 3000 |	2020-12-31
| IJK	     |   ALPHA	 |    1234	   | 2020-12-31	| 3000 |	9999-12-31

- See Filing XYZ for holder Alpha and security 1234 is reported on `2020-03-30`
- See Filing IJK for holder Alpha and security 1234 is also reported on `2020-03-30`

We want the unique combination of holder/security combinations, and sum the shares of the two.

```
CREATE TEMPORARY TABLE temp_thirteen_f_report_dates ON COMMIT DROP AS
SELECT DISTINCT
    holder_id
  , security_id
  , report_date
FROM temp_thirteen_f_intervals
;

INSERT INTO thirteen_f_shadow_table (
  SELECT
      report_dates.holder_id
    , report_dates.security_id
    , report_dates.report_date
    , SUM(intervals.shares) AS shares
  FROM temp_thirteen_f_report_dates AS report_dates
  INNER JOIN temp_thirteen_f_intervals AS intervals ON intervals.holder_id = report_dates.holder_id
    AND intervals.security_id = report_dates.security_id
    AND intervals.validity_interval @> report_dates.report_date
  GROUP BY
      report_dates.holder_id
    , report_dates.security_id
    , report_dates.report_date
  )
;
```

### Unionized Aggregated Holdings Interval Table
- This will union all the different types of holdings (13F, Stakes and Funds)
- We will delete all holdings that have a report date that is newer than the min changed report date (ie: we need to re-calculate everything for these holdings, because a holding at that date had a change, and could later affect any later holdings). These deleted holdings will be saved in a separate table to be used for later.
- After deleting, we will insert everything from the "shadow table" (aggregated_thirteen_f_holding_invervals) into the `aggregated_holding_intervals` table, which would contain all the properly calculated summed share values
- From there, we will need to re-calculate the valid until dates of these newly inserted holdings, which will be scoped to those that either have had a previous holding deleted (fetch from the deleted holdings table), or if the previous holding columns are null, because that means those were just inserted and need to be calculated
- Finally, we need to determine what the actual previous holding was. We do it by taking the most recent holding before the reported holding in question (that is max 3, 1 of 13F, Stake and Fund). Of those chosen, we determine which of those to choose (based on our expiry rules)


