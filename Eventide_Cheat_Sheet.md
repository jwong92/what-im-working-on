# Eventide Cheat Sheet
- See [here](http://docs.eventide-project.org/) for the up to date documentation
- See [here](http://docs.eventide-project.org/user-guide/libraries.html#top-level-libraries) for the all libraries that make up the eventide toolkit

## Eventide Command Line Component Generator
- See [here](https://github.com/eventide-project/command-line-component-generator)

1. Installation
`gem install evt-command_line-component_generator`
2. Usage
`evt component <something_component>`

- Don't forget to run the installation of the gems using `./install-gems.sh`

## Message DB
- See [here](https://github.com/message-db/message-db)

1. Installation
`gem install message-db`
2. Create the Postgres Database
`bundle exec mdb-create-db`

- Alternatively, if the component was made using the component generator, then you can also run `./db-setup.sh` which effectively does a drop and create of the message store db. Reminder that it is now a schema, and thus the table is under the namespace `message_store.messages`

### Command Line Tools
- See a list [here](http://docs.eventide-project.org/user-guide/message-db/tools.html#command-line-tools)
- Some common ones include:
```
bundle exec mdb-create-db
bundle exec mdb-delete-db
bundle exec mdb-recreate-db
bundle exec mdb-update-db
```

- A handy way to see messages being written is by using the command `mdb-print-messages` which will display the messages as they are being written into the message store
