# How the internet works

## What is a web browser

A web browser retrieves information from other parts of the web and displays it on your desktop or mobile device. It uses the HTTP (hypertext transfer protocol)
- HTTP defines how text images/videos are transmitted on the web
- Web browser fetches data from an internet connected server, and uses a software (rendering engine) to translate the data into text and images.
- The data being translated is HTML, which the browser reads and creates the web page

### Cookies
- Cookies are files that save information about you on your own computer for the next time you visit the site
- There are third party cookies that come from sites you haven't visited that tracks you from site-site to gather information which can be then sold to other companies.

### Render-Tree Construction
- CSSOM and DOM trees are combined into a render tree which is used to compute the layout of each visible element
- The CSSOM and DOM trees are built based on the HTML and CSS input. Both capture different aspects of the document: DOM describes the content, CSSOM describes the style rules applied to the document
- The render tree contains only the nodes required to render the page (ie: paint)

#### To Construct the Render Tree
![Render Tree](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/images/render-tree-construction.png)

1. Starting from the root of the DOM, traverse each visible node (some nodes are not visible ie: script/meta tags and are not reflected in the rendered output, some are hidden via CSS and also ommitted from the render tree - see display: none span)
2. For each visible node, find the appropriate matching CSSOM rule and apply them
3. Emit visible nodes with content/computed styles
4. The final output is a render containing both content/styl information of visible content on the screen
5. Up to here, only which nodes should be visible and computed styles have been computed...

#### Layout
- This is where we calculate the exact position and size of the viewport of the device.
- To determine, it begins at the root of the render tree and traverses it.
- The output of the layout process is a *box model* which captures the exact position and size of each element within the viewport: all relative measurements are converted to absolute pixels on the screen
- Since the render tree knows which nodes are visible, and their computed styles, the info is passed to the *painting* or *rasterizing* stage, where each node in the render tree is converted to actual pixels on the screen
- This step of *rasterizing* takes time, because the browser has a lot of work to do.

### CSSOM/DOM
- CSSOM (CSS Object Model) is a set of API's that allow the manipulation of CSS from javascript. It is the result of processing the CSS markup and building this CSSOM tree
- DOM (Document Object Model) - is the result of processing the HTML markup and building the DOM tree

#### DOM
The DOM is a programming interface for HTML / XML documents. A web page is a document, that can either be displayed in the browser window, or as the HTML source. The DOM represents that same document, but is written as an object orientated language so that it can be manipulated by javascript. The DOM is represented using nodes and objects.

The DOM standards are specified [here](https://dom.spec.whatwg.org/) and [here](https://dom.spec.whatwg.org/) which can be extended by browsers.

The DOM isn't a programming language, but it defines the HTML/XML document and their components (ie: elements). Each element in the document (ie: head, tables, table headers, text) is part of the DOM for that document, and can be accessed and manipulated using a scripting language like Javascript

`API = DOM + JavaScript`

The DOM is designed to be independent of any programming language, but structurally available from any single API. Implementations of the DOM can be built for any language


Rednering engine
HTTP protocol, other protocols?
DOM
CSSOM
nodes
