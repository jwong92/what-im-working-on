#!/usr/bin/env ruby

require 'pry'

class SubmarineNavigation
  HORIZONTAL_DIRECTION = 'forward'.freeze

  def initialize(**args)
    @x = 0
    @y = 0
    @aim = 0
    @instructions = args[:instructions]
  end

  def navigate
    @instructions.each do |instruction|
      # divides instruction into array ex: [forward, 1]
      direction, unit = instruction.split(' ')

      direction.include?(HORIZONTAL_DIRECTION) ? horizontal(direction, unit.to_i) : vertical(direction, unit.to_i)
    end

    return @x * @y
  end

  def horizontal(direction, unit)
    @x += unit
    @y += unit * @aim
  end

  def vertical(direction, unit)
    if direction == 'down'
      @aim += unit
    else
      @aim -= unit
    end
  end
end

file_lines = File.readlines("/Users/jwong/Documents/what-im-working-on/Advent_Of_Code_2021/data/2021-12-02.rb")

instructions = []

file_lines.each do |line|
  instructions << line.strip
end

coordinates = SubmarineNavigation.new({ instructions: instructions })
navigated = coordinates.navigate

puts "The final position is #{navigated}"