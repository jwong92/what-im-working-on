#!/usr/bin/env ruby

require 'pry'

class IncreasedDepthNumber
  def initialize(depths)
    @depths = depths
  end

  def increased
    num = 0

    @depths.map.with_index do |depth, i|
      num += 1 if depth > previous_depth(i) && i != 0
    end

    num
  end

  def previous_depth(idx)
    @depths[idx - 1]
  end
end

# returns an array like ["162\n", "150\n"...]
file_lines = File.readlines("/Users/jwong/Documents/what-im-working-on/A`dvent_Of_Code_2021/data/2021-12-01.rb")

depths = []
# strips newline characters and converts string to integers
file_lines.each do |line|
  depths << line.strip.to_i
end

puts "Total number of depths #{depths.count}"

increased_depth_number = IncreasedDepthNumber.new(depths)

# 1387
puts "There are #{increased_depth_number.increased} increased measurements"

# consider now the sum of a three measurement sliding window
sliding_depths = []
depths.map.with_index do |depth, i|
  sliding_depths << depth + depths[i+1] + depths[i+2] unless depths[i+1].nil? || depths[i+2].nil?
end

puts "There are #{sliding_depths.count} of grouped depths"

increased_depth_number = IncreasedDepthNumber.new(sliding_depths)

#1362
puts "There are #{increased_depth_number.increased} increased measurements"