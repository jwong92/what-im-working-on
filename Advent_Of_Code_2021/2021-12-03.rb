#!/usr/bin/env ruby
file_lines = File.readlines("/Users/jwong/Documents/what-im-working-on/Advent_Of_Code_2021/data/2021-12-03.rb")

binaries = []
file_lines.each { |line| binaries << line.strip }

class DiagnosticReport
  def initialize(**args)
    @binaries = args[:binaries]
    @gamma_binary = []
    @epsilon_binary = []
    @oxygen_generator_rating = ''
    @co2_scrubber_rating = ''
  end

  def power_consumption
    gamma_rate * epsilon_rate
  end

  def life_support_rating
    oxygen_generator_rating * co2_scrubber_rating
  end

  def oxygen_generator_rating
    binary_to_decimal(@oxygen_generator_rating)
  end

  def co2_scrubber_rating
    binary_to_decimal(@co2_scrubber_rating)
  end

  def gamma_rate
    binary_to_decimal(@gamma_binary.join(''))
  end

  def epsilon_rate
    binary_to_decimal(@epsilon_binary.join(''))
  end

  def diagnose
    Array(0..binary_length-1).map.with_index do |index|
      zero = []
      one = []

      @binaries.each do |binary|
        int = binary[index].to_i
        int == 0 ? zero << binary : one << binary
      end

      zero.count > one.count ? @gamma_binary << 0 : @gamma_binary << 1
      zero.count > one.count ? @epsilon_binary << 1 : @epsilon_binary << 0
    end
  end

  def generate_oxygen_rating
    binaries = @binaries

    Array(0..binary_length-1).map.with_index do |index|
      iterator = binary_iterator(binaries, index)
      zero = iterator[:zero]
      one = iterator[:one]

      zero.count > one.count ? binaries = zero : binaries = one

      return @oxygen_generator_rating = binaries.first if binaries.count == 1
    end
  end

  def generate_co2_rating
    binaries = @binaries

    Array(0..binary_length-1).map.with_index do |index|
      iterator = binary_iterator(binaries, index)
      zero = iterator[:zero]
      one = iterator[:one]

      zero.count > one.count ? binaries = one : binaries = zero

      return @co2_scrubber_rating = binaries.first if binaries.count == 1
    end
  end

  def binary_iterator(binaries, index)
    zero = []
    one = []

    binaries.each do |binary|
      int = binary[index].to_i
      int == 0 ? zero << binary : one << binary
    end

    return { zero: zero, one: one }
  end

  def binary_length
    @binaries[0].split('').count
  end

  def binary_to_decimal(binary)
    decimal = 0

    binary.to_s.split('').each { |int| decimal = decimal * 2 + int.to_i }

    decimal
  end
end

diagnostic_report = DiagnosticReport.new({ binaries: binaries })
diagnostic_report.diagnose
diagnostic_report.generate_oxygen_rating
diagnostic_report.generate_co2_rating

puts "power consumption #{diagnostic_report.power_consumption}"
puts "life support rating #{diagnostic_report.life_support_rating}"