# TradingView

## Overview
A charting tool that allows a user to share and view trading data. It can come included with real-time data and browser-based charts. No installation in required. Tradingview has a couple of options that are open-sourced, one of which is propriety, and what this will primarily talk about.

## Resources
* [Charting Library Example](https://charting-library.tradingview.com/)
* [Charting Library Wiki](https://github.com/tradingview/charting_library/wiki) (propriety - need to request access)
* [Charting Library Wiki](https://github.com/nikitamarcius/charting_library_wiki/wiki) (open source - copy of propriety)
* [Charting Library Github](https://github.com/tradingview) (contains examples)
* [Charting Library Guide](./files/TradingViewChartingLibraryDataAPIreference.pdf)
* [Charting Library Guide (Jon Church pt 1)](https://medium.com/@jonchurch/tradingview-charting-library-js-api-setup-for-crypto-part-1-57e37f5b3d5a)
* [Charting Library Guide (Jon Church pt 2)](https://medium.com/@jonchurch/tv-part-2-b8c2d4211f90)
* [Charting Library Guide (Nikita Marcius pt 1)](https://medium.com/marcius-studio/financial-charts-for-your-application-cfcceb147786)
* [Charting Library Guide (Nikita Marcius pt 2)](https://medium.com/marcius-studio/connecting-and-settings-tradingview-with-js-api-and-udf-adapter-b790297a31fa)
* [Charting Library Adapter Connection FAQ](https://medium.com/marcius-studio/connecting-and-settings-tradingview-with-js-api-and-udf-adapter-b790297a31fa)
* [Discord](https://discord.com/channels/641192799211094016/641204568172134400) (invite required)

## Charting Library
A standalone library (propriety) that can be downloaded and hosted on our servers. We can then connect our own data and use it in our application for free.

## Charting Library Package
```
    +/charting_library
        + /bundles
        - charting_library.js
        </del>- charting_library.d.ts</del>
        </del>- datafeed-api.d.ts</del>
    </del>+/datafeeds</del>
        </del>+ /udf</del>
    </del>- index.html</del>
    </del>- mobile_black.html</del>
    </del>- mobile_white.html</del>
    </del>- test.html</del>
```

The `charting_library/charting_library.js` contains the meat and potatoes of the chart, and from what I can tell has all the configuration methods that can be applied to the chart.

## Data Integration
The `charting_library` doesn't include market data, unless you use their widget options. Instead, they offer 2 ways for you to hook up your data to the chart. This also allows users to have more control over the chart.

![image info](./images/trading_view_charting_library_diagram.png)

### JS API
- Client Side Connection
This allows updating of the chart in real time, by connecting a type of data transport (websocket streaming, polling, or any other transport). There are a set of methods that must be implemented in JavaScript that will be called by the library as needed. This implementation is done in the datafeed of when we do the initial setup of the widget. Write your own adapter to the JS interface.

### UDF
- Server Side Connection
Without a Web-based server API that you can fetch data from, there is a ready-made UDF adapter that implements the JS API and makes simple HTTP(S) requests at the specified URL in a specific format. This adapter does not support data streaming out of the box (but it still can be added there).

## Widget Constructor
See the wiki [here](https://github.com/tradingview/charting_library/wiki/Widget-Constructor)

To initialize the Charting Library widget, these are the minimum parameters for calling the constructor.

```
new TradingView.widget({
    symbol: 'A',
    interval: '1D',
    container_id: "tv_chart_container",
    datafeed: new Datafeeds.UDFCompatibleDatafeed("https://demo_feed.tradingview.com")
});
```

- `symbol` - this is the default symbol of the chart, and calls the `resolveSymbol` function which should be defined in the datafeed. The examples all display symbol as a ticker or ticker-exchange. However, we will use the `listingPrimaryId` as a way to resolve the listing and display the prices on the chart.
- `interval` - this is the initial time period of one bar (aka resolution). The charting library supports intraday resolutions (seconds, minutes, hours) and daily, weekly monthly as well.
- `container_id` - is the id attribute name of the DOM element that contains the widget.
- `datafeed` - JS object containing the pre-defined interface used to feed the chart with data.

## Datafeed / JS API Adapter
- See the wiki [here](https://github.com/tradingview/charting_library/wiki/JS-Api)
- See also implementation example [here](https://github.com/tradingview/charting-library-tutorial/blob/master/documentation/datafeed-implementation.md)

The adapter contains methods that are executed in a specific order
- `onReady` => `resolveSymbol` => `getBars` => `subscribeBars` => `unsubscribeBars`
- After calling these functions, the datafeed will then return the results via callback functions

Example datafeed.js
```
export default {
    onReady: (callback) => {
      // Where you would configure the chart, such as the resolutions supported, exchanges, symbols etc...
      // Note that the callback needs to be called asynchronously
    },
    searchSymbols: (userInput, exchange, symbolType, onResultReadyCallback) => {
      // What you'd use to search symbols in the chart

      // onResultReadyCallback returns an object like
      //  {
      //    symbol: 'SPOT-NYS',
      //    full_name: 'Spotify Technology Inc',
      //    description: 'Spotify Technology Inc',
      //    exchange: 'NYS',
      //    ticker: <-- unique identifier -->,
      //    type: 'stock' | 'futures' | 'bitcoin' | 'forex' | 'index',
      //  }

      // The ticker is an unique identifier for this particular symbol.
      // If you specify this property then its value will be used for all data requests for this symbol.
      // This is where we will call the listings api to search by term
    },
    resolveSymbol: (symbolName, onSymbolResolvedCallback, onResolveErrorCallback) => {
      // Here, you can retrieve information about a specific symbol
      // symbolName is the equivalent of the ticker being passed from searchSymbols

      // Since we are only given the symbolName as a string (which we've changed to listingId)
      // we have to refetch the actual listing to return extra information on the listing
      // that will help the chart to properly display (ie. market open, scale, volume, time frame etc...)
    },
    getBars: (symbolInfo, resolution, from, to, onHistoryCallback, onErrorCallback, firstDataRequest) => {
      // This function is called when the chart needs a history fragment defined by date range.
      // from and to are defined by the chart itself, and can not be pre-defined

      // Instead, we define the date as a local state that we can manipulate ourselves.
      // We can keep track of what date is being displayed by subscribing to the user
      // changing the visible range.

      // Here, we call the listing prices api to fetch all prices for the given listingId

      // getBars will continuously be called until the visible range of the chart
      // is filled with data, unless we specify that there is no more data.

      // We use firstDataRequest to get all prices for a listing on the first request to
      // prevent getBars from calling the api over and over as the user scrolls

      // onHistoryCallback is expected to only be called once after all the data has been returned
    },
    subscribeBars: (symbolInfo, resolution, onRealtimeCallback, subscribeUID, onResetCacheNeededCallback) => {
      // This is called to subscribe to realtime updates.
      // You can call onRealtimeCallback any time a bar is changed with a new tick. (we only have EOD)

      // subscriberUID is the unique ID of a subscriber, formatted with the symbolInfo and the intraday
      // We can use this to keep track of what listings a user has selected to view on the chart
    },
    unsubscribeBars: (subscriberUID) => {
      // Keeps track of bars being unsubscribed, such as removing a listing from a chart
    },
};
```

## Other Customizations
- TradingView allows for custom studies, which is just a way of overlaying the chart with almostt anything your heart desires. To do that, they use the Pine Scripting Language found [here](https://www.tradingview.com/pine-script-reference/). The community has created a number of pre-defined scripts in pine that users can add to their charts as well.
