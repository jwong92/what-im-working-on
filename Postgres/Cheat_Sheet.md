# Postgres Brain Dump

## NOT IN vs NOT EXISTS

`NOT IN` is true when the value is not in the set returned by the subquery. `NOT EXISTS` is true when the subquery doesn't return anything.

## System

### version

- Display the current postgres version

```
SELECT VERSION()
```

- To view a list of functions available use `\df`

### Helpful Queries

`SELECT pg_size_pretty( pg_relation_size('tablename') );` - determines the size of a table (does not include indexes or additional objects)
`SELECT pg_size_pretty( pg_total_relation_size('tablename') );` - determines the size of a table _including_ indexes and additional objects
`SELECT pg_size_pretty (pg_indexes_size('tablename'));` - determines the size of a table's indexes
`SELECT pg_stat_activity WHERE state = 'active';` - determines actively running queries
`SELECT pg_cancel_backend(pid)` - terminates the query gien the pid
`NOT IN` is true when the value is not in the set returned by the subquery. `NOT EXISTS` is true when the subquery doesn't return anything.


## Extensions
- To add an extension `CREATE EXTENSION IF NOT EXISTS pgcrypto`

### pgcrypto
- See [here](https://www.postgresql.org/docs/12/pgcrypto.html) for documentation on the module pgcrypto

#### Digest()
```
digest(data test, type text)
```

## Postgres Optimization

Understanding memory allocation in postgres [here](https://www.postgresql.fastware.com/blog/back-to-basics-with-postgresql-memory-components)

`SHOW config_file` will determine the location of the configuration file

### Improving the performance of a INSERT/UPDATE/DELETE

#### INDEXES

- Won't help if you're essentially working on more than a certain percentage of the rows in the whole table. If there are 100 rows, and you're updating 99 of them, it is much easier to perform a sequence scan over those than an indexed on. The index won't be used, but instead be of hinderence, because after a row has been updated, then the index has to be created for it, which adds cost to your overall query performance.
- Sometimes, an index is not necessary, such as a PK index on the id, or can be emalgamated with another index, such as making a unique composite key the primary key.
- In postgres, an `UPDATE` is the same as an `INSERT`/`DELETE`. It will insert the new row, and mark the old row for destruction. This will leave you with a lot of dead tuples.

#### TEMPORARY TABLES

- Temp tables are not analyzed automatically. Tables created in the same transaction and used immediately don't give the `autovacuum` a chance to kick in. Thus, an `ANALYZE` must be run on the table manually, to avoid misguided query plans. Can also set the statistics target for irrelevant columns to 0
- `work_mem` and `temp_buffers` need to be properly allocated for CTE's and temp tables respectively, if there are more than a couple thousand rows that are being worked on. Anything less than ~1000 rows, should not touch any memory limits.
- To get an estimate of how much RAM is needed for a temporary table (and thus, how much you should set your `temp_buffers` to), you can run a small sample and use db object size funtions

```
SELECT pg_size_pretty(pg_relation_size('tmp_tbl'));  -- complete size of table
SELECT pg_column_size(t) FROM tmp_tbl t LIMIT 10;  -- size of sample rows
```

#### TEMPORARY TABLE VS CTE

_CTE's are better when:_

- Query planner can do a job of estimating how many rows will come out of it AND the contents of what those rows will be (if the planner's estimated vs actual rows from the CTE are way off, ie: 10x, then it is likely best to refactor into a temp table)
- When what comes out of the CTE doesn't _influence_ the behaviour of the rest of the query (ie: how many downstream queries are using the CTE? If they are being referenced multiple times, might be better to refactor into a temp table)
- You want to fence off a portion of a query - ie: if the planner isn't executing the query in a way that you believe is more efficient to be executed, like if you want to filter out rows first, and then have the query operate over that subset, then you might consider a CTE to fence off portions of the query
- When you're not sure what portions of the CTE's data will actually be necessary for the rest of the query (the planner can determine what parts to execute and ignore)

_Temp tables are better when:_

- You have to refer to the output multiple times
- You need to pass data between stored precedures
- You need to break a query up into phases to isolate unpredictable components that dramatically affect the behaviour of the rest fo the query.

#### JOINS

_FULL OUTER JOIN_

- First an inner join is performed, then for each row in T1 that does not satisfy the join condition with any row in T2, a joined row is added with null values in columns of T2.
- In addition, each row of T2 that does not satisfy the join condition with any row in T1, a joined row with null values in the columns of T1 is added

## psql

`\l` - To list all databases
`createdb <NAME>` - create a new postgres database
