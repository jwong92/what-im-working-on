# Holdings

## Holdings Algorithm

- We load anything after Jan 2017. FS can go back to the early 90's
- Internally, we have 13F's, Stakes, Sum of Funds
- 13F's - US regulation, any financial institution that holds 100 million USD or more must report their holdings, how many share values etc... (if you have a US presence at a scheduled interval quarterly.) If you were filing a 13F, you'd have to file an exit file that you're no longer 13f, then you don't have to again. Expires 8 months.
- Stakes - No strict single rule like the 13F. Expires 20 months
- Sum of funds - they get summed up to the institution. Ex: Fidelity has 5 funds, and two hold the same instiution, then we sum those funds. Expires 24 months

- Rules for which to choose either of those 3 filings depending on dates etc...

