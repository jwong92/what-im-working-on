# Arel
---
- For a brief introduction, start here: [url](https://www.rubydoc.info/gems/arel)
- For the source code of arel in rails, see [here](https://github.com/rails/rails/blob/main/activerecord/lib/arel.rb)

# Background
---
Arel is a library that is bundled as part of Rails. Arel is what the Rails framework (Active Record) uses under the hood in their ORM to ultimately be able to execute their SQL queries. Active Record provides a connection to the db, to allow a convenient way to specify relationships between models, as well as a nice query interface.

Arel can build queries in a flexible way, by building an AST (Abstract Syntax Tree). Arel operates on AST nodes (by modifying the query via method call) to create/modify the node(s) on the tree.

This manipulation of the AST nodes, allow composability, to allow building of the query iteratively, and combining several queries together, which would otherwise be difficult in AR.

## AST
A tree representation of the syntactic structure of source code written in a programming language. (ie: it's how a written piece of code would be visually executed by being drawn in a tree)

AST are data structures used in compilers to represent the structure of program code. It is the result of the syntax analysis phase of a compiler.

Benefits of AST in the compilation process
- Can be edited and enhanced with information like properties and annotations (not possible on source code since it would imply changes)
- Does *not* include inessenttial punctuation/delimiters (braces, semicolons, parentheses etc...)
- Contains extra info on the program due to stages of analysis by the compiler. Can store message position of each element in the source code to allow compiler to print useful error messages.

# Modules
---
## Arel::Table
This might not be necessary if the connection to the database has already been established via - rails AR

```
ActiveRecord::Base.establish_connection(adapter: "psql", database: ":memory:")
```

```
users = Arel::Table.new(:users, ActiveRecord::Base)

# => ActiveRecord::Base is optional, but specifies the engine in which the query is executed on, and specifies db details. Arel requires AR API's.
```

This creates an object representing the table itself. Note that the table can be named whatever you'd want it to be, and doesn't have to physically exist anywhere. This can be helpful for creating subqueries, or CTE's for exmaple.

[`Arel::Table`](https://github.com/rails/arel/blob/f50de54/lib/arel/table.rb#L45-L97) has many methods responsible for delegating calls to perform other actions in a query. Many of these methods are familiar to the AR ORM. One that may not be, is the `project` method, which is really a fancy way of saying select.

```
select_manager = users.project(Arel.star)

select_manager.to_sql
# => SELECT * FROM "users'
```

The [`Arel.star`](https://github.com/rails/rails/blob/main/activerecord/lib/arel.rb) method is simply a convenient method for the _*_ character. What is returned is a instance of [`Arel::SelectManager`](https://github.com/rails/arel/blob/f50de54/lib/arel/select_manager.rb).

```
select_manager = users.project(users[:id], users[:name])

select_manager.to_sql
# => SELECT "users"."id", "users"."name" FROM "users
```

The `Arel::Table#[]` method ultimately creates a new instance of `Arel::Attribute`, which itself includes a host of other methods, because of what it includes. (ie: `Arel/Expressions`, `Arel::Math` etc...)

The [`Arel::Attribute`](https://github.com/rails/rails/blob/main/activerecord/lib/arel/attributes/attribute.rb) represents a single column of an arbitrary name. The easiest way to capture an attribute for a table, is to use the [`Arel::Table#[]`](https://github.com/rails/arel/blob/f50de54/lib/arel/table.rb#L99-101) method.

```
select_manager = users.project(users[:comments_count].average)
select_manager.to_sql

# => SELECT AVG("users"."comments_count") AS avg_id FROM "users"
```

```
select_manager = users.project(users[:vip].as("status"), users[:vip].count.as("count")).group("vip")
select_manager.to_sql

# => SELECT "users"."vip" AS status, COUNT("users"."vip") AS count FROM "users"  GROUP BY vip
```

[`Arel::Expressions`](https://github.com/rails/rails/blob/9a263e9a0ffb82faa6d3153fd1f35b814a366cd5/activerecord/lib/arel/expressions.rb#L4) adds common aggregate functions (such as average or count), but would by default be a hardcoded name (ie: avg_id in this case). To alias it to a name that we'd like to use, there is [`Arel::AliasPredication`](https://github.com/rails/rails/blob/9a263e9a0ffb82faa6d3153fd1f35b814a366cd5/activerecord/lib/arel/alias_predication.rb#L4)


```
select_manager = users.project((users[:stared_comments_count] / users[:comments_count]).as("ratio"))
select_manager.to_sql
# => SELECT "users"."stared_comments_count" / "users"."comments_count" AS ratio FROM "users"
```

[`Arel::Math`](https://github.com/rails/arel/blob/f50de54/lib/arel/math.rb) is also useful to implement common math operators to be used directly on attributes as if we're working with values.

```
select_manager = users.project(Arel.star).where(users[:id].eq(23).or(users[:id].eq(42)))
select_manager = users.project(Arel.star).where(users[:id].eq_any([23, 42]))
select_manager.to_sql
# => SELECT * FROM "users"  WHERE ("users"."id" = 23 OR "users"."id" = 42)
```

```
admins_vips    = users[:admin].eq(true).or(users[:vip].eq(true))
with_karma     = users[:karma].gteq(5000).and(users[:hellbanned].eq(false))

select_manager = users.project(users[:id]).where(admins_vips.or(with_karma)).order(users[:id].desc)
select_manager.to_sql
# => SELECT COUNT("users"."id") FROM "users" WHERE (("users"."admin" = 't' OR "users"."vip" = 't')
#      OR "users"."karma" >= 5000 AND "users"."hellbanned" = 'f')
#    ORDER BY "users"."id" DESC
```

[`Arel::Predications`](https://github.com/rails/rails/blob/9a263e9a0ffb82faa6d3153fd1f35b814a366cd5/activerecord/lib/arel/predications.rb#L4) also has a host of methods to be used on the `Arel::Attribute` module, many of which are not found in AR's API's. Complicated queries can also be combined.
