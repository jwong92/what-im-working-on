# Shebang

Starting a script with `#!` is called using a `shebang` or `bang` line
- This is an absolute path to the `bash` interpreter
- All scripts under Linux execute using the interpreter specified on the first line
- All bash scripts begin with `#!bin/bash` assuming Bash has been installed in /bin
- This ensures bash will be used to interpret the script, even when executes in another shell

## Executables
- ls -al <PATH> will tell you a file's permissions. If there is the `x`, that means that the file is executable
- Linux can only run an executable if there are proper permissions (chmod)
- There are 2 types:
  1. Compiled programs (binaries)
  2. Scripts (which is where shebang comes into play)
- The extension of a file doesn't change how the script is executed

- Shebang tells the OS or shell how to evaluate the script
- It *must* be a full path to the executable.
- For example: `#!/usr/bin/python3` or `#!/usr/bin/ruby`

## /usr/bin/env bash
- The /usr/bin/env run a program in a modified environment. It makes the bash script portable. It will use whattever bash executable that appears first in the user's `$PATH` variable
- This comes in handy (ie: using the env file) when we are not hardcoding the absolute path to the users executable.
- Using env, and then specifying the executable, means that it can be executed elsewhere properly

## Limitations
- shebang's max out at 127 characters
- Only supported in linux

## Windows
- To run an executable, they run in 3 parts
- They use `$PATH` to determine where to run the executable
- The current working directory (CWD)
- They also use `%PATHEXT%` to be able to determine which ones can run
